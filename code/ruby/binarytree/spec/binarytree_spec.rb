require 'spec_helper'

describe Binarytree::BinaryTree do
  it 'Allows node insertion' do
    my_binary_tree = Binarytree::BinaryTree.new

    nodes = [
        Binarytree::BinaryTreeNode.new(10),
        Binarytree::BinaryTreeNode.new(4),
        Binarytree::BinaryTreeNode.new(9),
        Binarytree::BinaryTreeNode.new(3)
    ]

    nodes.each do |node|
      my_binary_tree.insert(node)
    end

    expected = [nodes[3], nodes[1], nodes[2], nodes[0]]

    expect(my_binary_tree.root.inorder_traversal).to eq(expected)
  end

  it 'Allows node deletion' do
    my_binary_tree = Binarytree::BinaryTree.new

    root_node = Binarytree::BinaryTreeNode.new(10)

    child_nodes = [
        Binarytree::BinaryTreeNode.new(4),
        Binarytree::BinaryTreeNode.new(9),
        Binarytree::BinaryTreeNode.new(3)
    ]

    my_binary_tree.insert(root_node)

    child_nodes.each do |node|
      my_binary_tree.insert(node)
    end

    my_binary_tree.delete(child_nodes[1])

    expected = [child_nodes[2], child_nodes[0], root_node]

    expect(my_binary_tree.root.inorder_traversal).to eq(expected)
  end
end
