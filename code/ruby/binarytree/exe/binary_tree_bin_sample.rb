#!/usr/bin/env ruby

require 'binarytree'

my_binary_tree = Binarytree::BinaryTree.new

root_node = Binarytree::BinaryTreeNode.new(10)
my_binary_tree.insert(root_node)

8.upto(13).each do |num|
  my_binary_tree.insert(Binarytree::BinaryTreeNode.new(num))
end

my_binary_tree.pretty_print

found = my_binary_tree.search(9)
my_binary_tree.delete(found)
my_binary_tree.delete(root_node)

my_binary_tree.pretty_print