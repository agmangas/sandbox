require 'awesome_print'

module Binarytree
  class BinaryTree
    attr_reader :root

    def initialize
      @root = nil
    end

    def to_s
      if @root.nil?
        'Empty tree'
      else
        traversed = @root.inorder_traversal.map { |node| node.inspect }
        traversed.join("\n\n").to_s
        traversed
      end
    end

    def pretty_print
      ap @root.inorder_traversal
    end

    def insert(node)
      if @root.nil?
        @root = node
      else
        @root.insert(node)
      end
    end

    def search(item)
      current = @root

      while !current.nil?
        if current.item == item
          return current
        else
          child_getter = item < current.item ? 'lchild' : 'rchild'
          child = current.send(child_getter.to_sym)
          if child.nil?
            break
          else
            current = child
          end
        end
      end

      raise StandardError("Couldn't find the item in the tree")
    end

    def delete(node)
      raise StandardError('Unknown node') unless @root.inorder_traversal.include? node

      if node.eql? @root
        if @root.children_count == 2
          @root.delete
        elsif children_count == 1
          root_child = (@root.lchild.nil?) ? @root.rchild : @root.lchild
          root_child.parent = nil
          @root = root_child
        else
          @root = nil
        end
      else
        node.delete
      end
    end
  end
end