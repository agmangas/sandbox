module Binarytree
  class BinaryTreeNode
    attr_accessor :item, :parent, :lchild, :rchild

    def initialize(item)
      @item = item
      @parent = nil
      @lchild = nil
      @rchild = nil
    end

    def insert(node)
      child = node.item < @item ? '@lchild' : '@rchild'
      if instance_variable_get(child).nil?
        instance_variable_set(child, node)
        node.parent = self
      else
        instance_variable_get(child).insert(node)
      end
    end

    def children_count
      count = 0
      [@lchild, @rchild].each do |child|
        count += 1 unless child.nil?
      end
      count
    end

    def successor
      first_successor = @rchild
      if first_successor.nil?
        return nil
      else
        current = first_successor
        while !current.nil?
          if current.lchild.nil?
            return current
          else
            current = current.lchild
          end
        end
      end
    end

    def parent_self_sym
      return nil if @parent.nil?
      return (@parent.lchild == self) ? :lchild : :rchild
    end

    def delete
      if children_count == 2
        successor_node = successor
        @item = successor_node.item
        successor_node.delete
      elsif children_count == 1
        self_child = (@lchild.nil?) ? @rchild : @lchild
        @parent.send((parent_self_sym.to_s + '=').to_sym, self_child)
      else
        @parent.send((parent_self_sym.to_s + '=').to_sym, nil)
      end
    end

    def inorder_traversal
      nodes = []

      unless @lchild.nil?
        nodes += @lchild.inorder_traversal
      end

      nodes += [self]

      unless @rchild.nil?
        nodes += @rchild.inorder_traversal
      end

      nodes
    end
  end
end