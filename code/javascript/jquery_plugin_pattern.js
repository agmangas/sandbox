(function ($) {

    var methods = {
        init: function (options) {
            var settings = $.extend({
                defaultOpt: 10
            }, options);
            this.data("settings", settings);
            return this;
        },
        dummyLog: function () {
            console.log("My Method!");
            return this;
        }
    };

    $.fn.myPlugin = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method == "object") {
            return methods["init"].apply(this, arguments);
        } else {
            $.error("Method not found");
        }
    };

})(jQuery);