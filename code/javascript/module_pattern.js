var Calculator = (function () {
    var currentResult;
    return {
        sum: function (x, y) {
            currentResult = x + y;
            return currentResult;
        },
        multiply: function (x, y) {
            currentResult = x * y;
            return currentResult;
        },
        result: function () {
            return currentResult;
        },
        clear: function () {
            currentResult = undefined;
        }
    }
});

var myCalculator, x, y;

myCalculator = Calculator();
x = 4;
y = 3;

console.log(">> x + y = " + myCalculator.sum(x, y));
console.log(">> Last result: " + myCalculator.result());
console.log(">> x * y = " + myCalculator.multiply(x, y));
console.log(">> Last result: " + myCalculator.result());
console.log(">> Clearing result");
myCalculator.clear();
console.log(">> Last result: " + myCalculator.result());