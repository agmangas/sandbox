"""
Implementations and tests for multiple sorting algorithms.
Usage: python sorting.py
"""

import random
import math

from heap import BinaryMaxHeap


def quicksort_partition(lst):
    """Implementation of the partition step of the Quicksort algorithm."""

    # Choose the middle element as the pivot

    pivot_idx = len(lst) / 2
    pivot_val = lst[pivot_idx]

    # Swap the pivot with the rightmost element

    lst[pivot_idx], lst[-1] = lst[-1], lst[pivot_idx]

    # Iterate over the list from 0 to len - 1
    # When the current element is lower than the pivot value:
    # Swap the current element with the store index and increase store index by 1

    store_idx = 0

    for idx, val in enumerate(lst[:-1]):
        if val < pivot_val:
            lst[idx], lst[store_idx] = lst[store_idx], lst[idx]
            store_idx += 1

    # Swap the store index element with the rightmost element (pivot)

    lst[store_idx], lst[-1] = lst[-1], lst[store_idx]

    return store_idx


def quicksort(lst):
    """Quicksort implementation.
    Best / Avg / Worst : O(nlogn) / O(nlogn) / O(n2).
    Typically, quicksort is significantly faster in practice than other O(nlogn) algorithms,
    because its inner loop can be efficiently implemented on most architectures, and in most
    real-world data, it is possible to make design choices which minimize the probability of
    requiring quadratic time."""

    if len(lst) < 2:
        return lst

    part_idx = quicksort_partition(lst)
    sorted_left = quicksort(lst[:part_idx])
    sorted_right = quicksort(lst[part_idx + 1:])

    return sorted_left + [lst[part_idx]] + sorted_right


def merge(lft, rgt):
    """Implementation of the merge step of the Mergesort algorithm."""

    ret = list()

    while len(lft) and len(rgt):
        if lft[0] <= rgt[0]:
            ret.append(lft[0])
            lft.pop(0)
        else:
            ret.append(rgt[0])
            rgt.pop(0)

    if len(lft):
        ret += lft

    if len(rgt):
        ret += rgt

    return ret


def mergesort(lst):
    """Mergesort implementation.
    Best / Avg / Worst : O(nlogn) / O(nlogn) / O(nlogn)."""

    if len(lst) < 2:
        return lst

    pivot_idx = len(lst) / 2
    lst_lft = lst[:pivot_idx]
    lst_rgt = lst[pivot_idx:]

    lst_lft_sorted = mergesort(lst_lft)
    lst_rgt_sorted = mergesort(lst_rgt)

    return merge(lst_lft_sorted, lst_rgt_sorted)


def heapify(lst):
    """Transform the input list so the resulting list is a heap.
    A heap is an array-like that contains the layout of a nearly complete binary tree.
    In this case the final heap is also a max-heap, which means that the values of the
    children of any node are lower than the value of said node."""

    # start is assigned the index in 'lst' of the last parent node
    # The last element in a 0-based array is at index len(lst) - 1; find the parent of that element

    start = int(math.floor(float(len(lst) - 2) / 2))

    while start >= 0:
        # Sift down the node at index 'start' to the proper place such that
        # all nodes below the start index are in heap order

        sift_down(lst, start, len(lst) - 1)

        # Go to the next parent node

        start -= 1

    # After sifting down the root all nodes/elements are in heap order

    pass


def sift_down(lst, start, end):
    """Repair the heap whose root element is at index 'start', assuming the heaps
    rooted at its children are valid."""

    root = start

    # While the root has at least one child

    while root * 2 + 1 <= end:
        left_child_idx = root * 2 + 1  # Left child
        swap_idx = root  # Keeps track of child to swap with

        if lst[swap_idx] < lst[left_child_idx]:
            swap_idx = left_child_idx

        # If there is a right child and that child is greater

        if left_child_idx + 1 <= end and lst[swap_idx] < lst[left_child_idx + 1]:
            swap_idx = left_child_idx + 1

        # If swap index is equal to root index:
        # The root holds the largest element
        # Since we assume the heaps rooted at the children are valid,
        # this means that we are done

        if swap_idx == root:
            return
        else:
            lst[root], lst[swap_idx] = lst[swap_idx], lst[root]
            root = swap_idx


def heapsort(lst):
    """Heapsort implementation.
    Best / Avg / Worst : O(nlogn) / O(nlogn) / O(nlogn)."""

    ret = lst[:]

    # Build the heap in array ret so that largest value is at the root

    heapify(ret)

    # The following loop maintains the invariants that ret[0:end] is a heap
    # and every element beyond end is greater than everything before it
    # (so ret[end:count] is in sorted order)

    end = len(lst) - 1

    while end > 0:
        # ret[0] is the root and largest value.
        # The swap moves it in front of the sorted elements

        ret[end], ret[0] = ret[0], ret[end]

        # The heap size is reduced by one

        end -= 1

        # The swap ruined the heap property, so restore it

        sift_down(ret, 0, end)

    return ret


def heapsort_alt(lst):
    """Alternative heapsort implementation."""

    heap, ret = BinaryMaxHeap(), list()

    for val in lst:
        heap.insert(val)

    while not heap.is_empty():
        ret.append(heap.pop())

    return list(reversed(ret))


if __name__ == "__main__":

    algorithm_impls = [
        quicksort,
        mergesort,
        heapsort,
        heapsort_alt
    ]

    test_list = range(25)
    random.shuffle(test_list)
    test_list_ok = sorted(test_list)

    for impl in algorithm_impls:
        print(">> Testing {}".format(impl))
        test_list_copy = test_list[:]
        test_list_sorted = impl(test_list_copy)
        assert test_list_ok == test_list_sorted
        print(">> {} OK".format(impl))
