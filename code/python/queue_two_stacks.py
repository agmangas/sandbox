"""
Question extracted from: https://www.interviewcake.com/question/queue-two-stacks

Implement a queue with 2 stacks. Your queue should have an enqueue and a dequeue function and it
should be "first in first out" (FIFO). Optimize for the time cost of mm function calls on your queue.
These can be any mix of enqueue and dequeue calls.

Assume you already have a stack implementation and it gives O(1) time push and pop.
"""


class QueueTwoStacks(object):
    """Queue implementation using two stacks with O(1) push and pop."""

    def __init__(self):
        """Constructor."""

        self.inbox = list()
        self.outbox = list()

    def enqueue(self, item):
        """Queue an object in the list: O(1)."""

        self.inbox.append(item)

    def dequeue(self):
        """Dequeue an object from the list: O(1) amortized."""

        if not len(self.outbox):
            while len(self.inbox):
                self.outbox.append(self.inbox.pop())

        return self.outbox.pop()
