"""
Implementation of combinatorial search method backtracking.
"""

import pprint


def back_permut_is_solution(curr_candidate, data):
    """Implementation of the 'is_solution' function for the
    backtracking version of a permutations generator.
    Checks if the current candidate is a valid solution."""

    return True if len(curr_candidate) == data["n"] else False


# noinspection PyUnusedLocal
def back_permut_build_options(curr_candidate, idx, data):
    """Implementation of the 'build_options' function for
    the backtracking version of a permutations generator.
    Returns a list that contains all the possible options built from the current candidate"""

    used, available = list(curr_candidate), data["x"][:]

    for idx in range(len(used)):
        available.remove(used.pop())

    options = list()

    for element in available:
        option = list(curr_candidate)
        option.append(element)
        options.append(tuple(option))

    return options


def back_permut_process_solution(curr_candidate, data):
    """Implementation of the 'process_solution' function for the
    backtracking version of a permutations generator.
    Processes the current candidate, which is a valid solution."""

    data["ret"].add(curr_candidate)


def backtracking(curr_candidate, idx, is_solution, build_options, process_solution, data):
    """Backtracking algorithm implementation."""

    if is_solution(curr_candidate, data):
        process_solution(curr_candidate, data)
    else:
        new_candidates = build_options(curr_candidate, idx, data)
        for candidate in new_candidates:
            backtracking(
                candidate, idx + 1, is_solution,
                build_options, process_solution, data)


if __name__ == "__main__":
    """Entry point."""

    permut_data = {
        "n": 2,
        "x": [1, 2, 2, 3, 4, 5, 6],
        "ret": set()
    }

    print(">> Permutations of {} taken in groups of {}".format(
        permut_data["x"], permut_data["n"]))

    backtracking(
        list(), 0, back_permut_is_solution, back_permut_build_options,
        back_permut_process_solution, permut_data)

    print("{}".format(pprint.pformat(permut_data["ret"])))