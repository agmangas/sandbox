"""
Graph / Tree search algorithms implementations.
"""


def breadth_first_search(graph, root):
    """Takes an adjacency list graph and the root node and
    searches all its nodes using BFS."""

    nodes_seen = set()
    nodes_queue = list()
    nodes_queue.append(root)
    nodes_seen.add(root)

    while len(nodes_queue):
        current_node = nodes_queue.pop(0)
        print("Found: {}".format(current_node))
        for child in graph[current_node]:
            if child in nodes_seen:
                continue
            nodes_queue.append(child)
            nodes_seen.add(child)


def depth_first_search(graph, root, verbose=True, searched=None, directed=False):
    """Takes an adjacency list graph and the root node and
    searches all its nodes using DFS."""

    nodes_seen = set()
    nodes_processed = set()
    nodes_stack = list()
    nodes_stack.append((root, 0))
    nodes_seen.add(root)

    while len(nodes_stack):

        current_node, current_node_level = nodes_stack.pop()
        nodes_processed.add(current_node)

        if verbose:
            print("Node: {}".format(current_node))

        if current_node == searched:
            return current_node_level

        for child in graph[current_node]:

            if (not child in nodes_processed or directed) and verbose:
                arrow = "->" if directed else "<->"
                print("Edge: {} {} {}".format(current_node, arrow, child))

            if not child in nodes_seen:
                nodes_stack.append((child, current_node_level + 1))
                nodes_seen.add(child)

    return None


def graph_distance(graph, start, goal):
    """Returns the number of edges between node 'start' and node 'goal'."""

    distance = depth_first_search(graph, start, searched=goal, verbose=False)

    if not distance:
        return float(10e3)

    return float(distance)


def a_star(graph, start, goal):
    """A* graphs path searching algorithm implementation."""

    closedset = set()
    openset = {start}
    came_from = dict()

    g_score, f_score = dict(), dict()
    g_score[start] = 0
    f_score[start] = g_score[start] + graph_distance(graph, start, goal)

    while len(openset):
        current = sorted(list(openset), key=lambda nde: f_score.get(nde, float("inf")))[0]
        if current == goal:
            return _reconstruct_path(came_from, current)

        openset -= {current}
        closedset.add(current)
        for neighbor in graph[current]:
            if neighbor in closedset:
                continue
            tentative_g_score = g_score[current] + 1.0

            if neighbor not in openset or tentative_g_score < g_score[neighbor]:
                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = g_score[neighbor] + graph_distance(graph, neighbor, goal)
                if neighbor not in openset:
                    openset.add(neighbor)

    raise Exception("Could not find path")


def dijkstra(graph, start, goal, weights=None):
    """Dijkstra's shortest path algorithm implementation."""

    weights = weights if weights is not None else dict()

    nodes_set = set(graph.keys())
    dists = dict((node, float("inf")) for node in nodes_set)
    dists[start] = 0
    shortest_path = list()

    while True:

        curr_node = min(nodes_set, key=lambda k: dists[k])
        nodes_set -= {curr_node}
        shortest_path.append(curr_node)

        for neighbor in graph[curr_node]:
            neigh_w = weights.get((neighbor, curr_node), weights.get((curr_node, neighbor), 1))
            alt_dist = dists[curr_node] + neigh_w
            if alt_dist < dists[neighbor]:
                dists[neighbor] = alt_dist

        if curr_node == goal:
            break

    return shortest_path


def _reconstruct_path(came_from, current):
    """Reconstructs the followed path in the A* algorithm."""

    total_path = [current]

    while current in came_from:
        current = came_from[current]
        total_path.append(current)

    return total_path


if __name__ == "__main__":
    """Entry point."""

    graph_directed = {
        "Frankfurt": ["Mannheim", "Wurzburg", "Kassel"],
        "Mannheim": ["Karlsruhe"],
        "Wurzburg": ["Erfurt", "Numberg"],
        "Kassel": ["Munchen"],
        "Karlsruhe": ["Augsburg"],
        "Erfurt": [],
        "Numberg": ["Munchen", "Stuttgart"],
        "Munchen": [],
        "Augsburg": ["Munchen"],
        "Stuttgart": []
    }

    graph_undirected = {
        "Frankfurt": ["Mannheim", "Wurzburg", "Kassel"],
        "Mannheim": ["Karlsruhe", "Frankfurt"],
        "Wurzburg": ["Erfurt", "Numberg", "Frankfurt"],
        "Kassel": ["Munchen", "Frankfurt"],
        "Karlsruhe": ["Augsburg", "Mannheim"],
        "Erfurt": ["Wurzburg"],
        "Numberg": ["Munchen", "Stuttgart", "Wurzburg"],
        "Munchen": ["Augsburg", "Numberg", "Kassel"],
        "Augsburg": ["Munchen", "Karlsruhe"],
        "Stuttgart": ["Numberg"]
    }

    print(">> Breadth-first search")
    print("***********************")

    breadth_first_search(graph_directed, "Frankfurt")

    print(">> Depth-first search (Directed)")
    print("********************************")

    depth_first_search(graph_directed, "Frankfurt", directed=True)

    print(">> Depth-first search (Undirected)")
    print("**********************************")

    depth_first_search(graph_undirected, "Frankfurt")

    print(">> A* path searching")
    print("********************")

    node_from, node_to = "Karlsruhe", "Munchen"

    try:
        path = reversed(a_star(graph_directed, node_from, node_to))
        print("From {} to {}: {}".format(node_from, node_to, " > ".join(path)))
    except Exception:
        print("No path from {} to {}".format(node_from, node_to))

    print(">> Dijkstra path searching")
    print("********************")

    node_from, node_to = "Karlsruhe", "Munchen"

    try:
        path = dijkstra(graph_directed, node_from, node_to)
        print("From {} to {}: {}".format(node_from, node_to, " > ".join(path)))
    except Exception:
        print("No path from {} to {}".format(node_from, node_to))