"""
Kd-tree implementation.
"""

import math


class KdTreeNode(object):
    """Kd-tree node."""

    def __init__(self, x, y):
        """Constructor."""

        self.x = x
        self.y = y
        self.lchild = None
        self.rchild = None

    def __repr__(self):
        """Object's representation."""

        return "Node [{}] {} -> L:{} / R:{}".format(
            id(self), self.coords(),
            id(self.lchild) if self.lchild else "NULL",
            id(self.rchild) if self.rchild else "NULL")

    def coords(self):
        """Returns the coordinates tuple."""

        return self.x, self.y

    def insert(self, lcoords, rcoords, depth):
        """Insert the given coordinates under this node."""

        depth_idx = depth % KdTree.NUM_DIMS

        if len(lcoords) > 1:
            lmedian_idx = KdTree.find_median_idx(lcoords, depth_idx)
            self.lchild = KdTreeNode(*lcoords[lmedian_idx])
            lcoords_split = KdTree.split_median(lcoords, lmedian_idx, depth_idx)
            self.lchild.insert(lcoords_split[0], lcoords_split[1], depth_idx + 1)
        elif len(lcoords) == 1:
            self.lchild = KdTreeNode(*lcoords[0])

        if len(rcoords) > 1:
            rmedian_idx = KdTree.find_median_idx(rcoords, depth_idx)
            self.rchild = KdTreeNode(*rcoords[rmedian_idx])
            rcoords_split = KdTree.split_median(rcoords, rmedian_idx, depth_idx)
            self.rchild.insert(rcoords_split[0], rcoords_split[1], depth_idx + 1)
        elif len(rcoords) == 1:
            self.rchild = KdTreeNode(*rcoords[0])

    def preorder_iterator(self):
        """Pre-order iterator."""

        yield self

        if self.lchild is not None:
            for lnode in self.lchild.preorder_iterator():
                yield lnode

        if self.rchild is not None:
            for rnode in self.rchild.preorder_iterator():
                yield rnode

    def nearest_neighbor(self, query, depth=0):
        """Find the nearest neighbor under this node."""

        curr_best, rewind_path = self, list()

        while True:
            depth_idx = depth % KdTree.NUM_DIMS
            if query[depth_idx] <= curr_best.coords()[depth_idx]:
                if curr_best.lchild is None:
                    break
                else:
                    rewind_path.append((curr_best, curr_best.rchild, depth))
                    curr_best = curr_best.lchild
                    depth += 1
            else:
                if curr_best.rchild is None:
                    break
                else:
                    rewind_path.append((curr_best, curr_best.lchild, depth))
                    curr_best = curr_best.rchild
                    depth += 1

        query_dist = lambda crd: abs(query[0] - crd[0]) + abs(query[1] - crd[1])

        while len(rewind_path):
            rnode, rnode_child, rnode_depth = rewind_path.pop()
            if rnode is None:
                continue
            if query_dist(curr_best.coords()) > query_dist(rnode.coords()):
                curr_best = rnode
                if rnode_child is not None:
                    candidate = rnode_child.nearest_neighbor(query, rnode_depth + 1)
                    if query_dist(curr_best.coords()) > query_dist(candidate.coords()):
                        curr_best = candidate

        return curr_best


class KdTree(object):
    """Kd-tree implementation.
    Useful for nearest neighbour finding and range searches."""

    NUM_DIMS = 2

    def __init__(self):
        """Constructor."""

        self.root = None

    @classmethod
    def find_median_idx(cls, coords, depth_idx):
        """Returns the median coordinates index for the given depth."""

        sorted_coords = sorted(coords, key=lambda c: c[depth_idx])
        median_idx = int(math.floor(float(len(sorted_coords)) / 2))
        median_coords = sorted_coords[median_idx]

        for idx in xrange(len(coords)):
            if coords[idx] == median_coords:
                return idx

    @classmethod
    def split_median(cls, coords, median_idx, depth_idx):
        """Splits the coordinates list into coordinates to the left of
        the median and coordinates to its right."""

        lcoords, rcoords = list(), list()
        median_val = coords[median_idx][depth_idx]

        for idx, coord in enumerate(coords):
            if idx == median_idx:
                continue
            if coord[depth_idx] <= median_val:
                lcoords.append(coord)
            else:
                rcoords.append(coord)

        return lcoords, rcoords

    def build(self, coords):
        """Takes a list of coordinates (tuples) and builds the KdTree."""

        root_median_idx = self.find_median_idx(coords, 0)
        self.root = KdTreeNode(*coords[root_median_idx])
        coords_split = self.split_median(coords, root_median_idx, 0)
        self.root.insert(coords_split[0], coords_split[1], 1)

    def preorder_iterator(self):
        """Pre-order iterator."""

        return self.root.preorder_iterator()

    def nearest_neighbor(self, query):
        """Find the nearest neighbor to the query point."""

        return self.root.nearest_neighbor(query)


if __name__ == "__main__":
    """Entry point."""

    test_coords = [
        (2, 3),
        (5, 4),
        (9, 6),
        (4, 7),
        (8, 1),
        (7, 2)
    ]

    print(">> Building the Kd-Tree")

    kdtree = KdTree()
    kdtree.build(test_coords)

    print(">> Pre-order traversal:")

    for knode in kdtree.preorder_iterator():
        print knode

    nn_test = (7.7, 3.0)

    print(">> Nearest neighbor to {}: {}".format(
        nn_test, kdtree.nearest_neighbor(nn_test)))