"""
Question extracted from: https://www.interviewcake.com/question/permutation-palindrome

Write an efficient function that checks whether any permutation of an input string is a palindrome.

Examples:

"civic" should return true
"ivicc" should return true
"civil" should return false
"livci" should return false
"""

import math
from collections import Counter


def is_palindrome(strng):
    """Returns True if the argument is a palindrome."""

    max_check_idx = int(math.floor(len(strng) / 2.0))

    for idx in range(max_check_idx):
        if strng[idx] != strng[-1 - idx]:
            return False

    return True


def bf_has_palindromes(orig_strng, curr_strng=None, results=None):
    """Returns True if any permutation of the input string is a palindrome."""

    curr_strng = "" if curr_strng is None else curr_strng
    results = list() if results is None else results

    if len(curr_strng) == len(orig_strng):
        results.append(curr_strng)
    else:
        available_chars = [c for c in orig_strng]
        for ch in curr_strng:
            available_chars.pop(available_chars.index(ch))
        for ch in available_chars:
            new_str = curr_strng + ch
            if not new_str in results:
                bf_has_palindromes(orig_strng, curr_strng=new_str, results=results)

    return True if True in [is_palindrome(res) for res in results] else False


def has_palindromes(orig_strng):
    """Returns True if any permutation of the input string is a palindrome."""

    counts = dict()

    for ch in orig_strng:
        if not ch in counts:
            counts[ch] = 1
        else:
            counts[ch] += 1

    counts_counter = Counter(counts.values())

    if len(orig_strng) % 2 == 1:
        try:
            assert counts_counter[1] == 1
            assert counts_counter[2] == math.floor(len(orig_strng) / 2.0)
            return True
        except AssertionError:
            return False
    else:
        try:
            assert counts_counter[2] == len(orig_strng)
            return True
        except AssertionError:
            return False


if __name__ == "__main__":
    """Entry point."""

    test_strngs = [
        "civic",
        "ivicc",
        "civil",
        "livci"
    ]

    print(">> Palindromes?: {}".format(
        [(x, is_palindrome(x)) for x in test_strngs]))

    print(">> Brute force approach")

    for test_strng in test_strngs:
        print(">> Testing: {}".format(test_strng))
        permuts = list()
        has_palindrome = bf_has_palindromes(test_strng, results=permuts)
        print(">> Has palindromes?: {}".format(has_palindrome))
        print(">> Permutations: {}".format(permuts))

    print(">> Efficient approach")

    for test_strng in test_strngs:
        print(">> Testing: {}".format(test_strng))
        print(">> Has palindromes?: {}".format(has_palindromes(test_strng)))
