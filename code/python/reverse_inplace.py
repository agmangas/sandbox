"""
Various inplace reversing algorithms.
"""

import math


class LinkedListNode(object):
    def __init__(self, item):
        self.item = item
        self.next_node = None

    def __repr__(self):
        return "Node: {} ({}) -> {}".format(
            id(self), self.item, id(self.next_node) if self.next_node is not None else "NULL")


def _print_linked_list(headn):
    """Takes the head node and prints the entire linked list."""

    currn = headn
    while currn is not None:
        print "## {}".format(currn)
        currn = currn.next_node


def _create_linked_list():
    """Creates a test linked list."""

    headn = LinkedListNode(1)
    currn = headn

    for i in range(2, 6):
        currn.next_node = LinkedListNode(i)
        currn = currn.next_node

    return headn


def reverse_inplace_string(input_str):
    """Takes an input_str string and returns the in-place reversed version.
    Due to Python not having mutable strings an intermediate character array is created."""

    chars = list(input_str)

    pivot_ini, pivot_end = 0, len(chars) - 1
    middle_idx = int(math.floor(len(chars) / 2.0))

    for idx in range(middle_idx):
        curr_ini = pivot_ini + idx
        curr_end = pivot_end - idx
        chars[curr_ini], chars[curr_end] = chars[curr_end], chars[curr_ini]

    return "".join(chars)


def reverse_inplace_linkedlist(headn):
    """Reverses a linked list in place.
    Returns the head_node of the reversed list."""

    if headn.next_node is None:
        return headn

    prev, curr, advc = None, headn, headn.next_node

    headn.next_node = None

    while advc is not None:
        prev = curr
        curr = advc
        advc = advc.next_node
        curr.next_node = prev

    return curr


def reverse_inplace_linkedlist_alt(headn):
    """Reverses a linked list in place (alternative version).
    Returns the head_node of the reversed list."""

    new_headn = None

    while headn is not None:
        tmp_next = headn.next_node
        headn.next_node = new_headn
        new_headn = headn
        headn = tmp_next

    return new_headn


if __name__ == "__main__":
    test_str_1 = "Hello there!"
    test_str_1_rev = reverse_inplace_string(test_str_1)
    print(">> Reverse of {}: {}".format(test_str_1, test_str_1_rev))
    assert test_str_1_rev == "!ereht olleH"

    test_str_2 = "Let's go the beach!"
    test_str_2_rev = reverse_inplace_string(test_str_2)
    print(">> Reverse of {}: {}".format(test_str_2, test_str_2_rev))
    assert test_str_2_rev == "!hcaeb eht og s'teL"

    head_node = _create_linked_list()
    print(">> Original linked list")
    _print_linked_list(head_node)
    head_node_rev = reverse_inplace_linkedlist(head_node)
    print(">> Reversed linked list")
    _print_linked_list(head_node_rev)

    head_node_alt = _create_linked_list()
    print(">> Original linked list (Alt)")
    _print_linked_list(head_node_alt)
    head_node_alt_rev = reverse_inplace_linkedlist_alt(head_node_alt)
    print(">> Reversed linked list (Alt)")
    _print_linked_list(head_node_alt_rev)