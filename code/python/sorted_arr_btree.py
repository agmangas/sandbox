"""
Build a balanced binary tree from a sorted array.
"""

import math


class BinaryTreeNode(object):
    """Basic binary tree node."""

    def __init__(self, item):
        """Constructor."""

        self.item = item
        self.lchild = None
        self.rchild = None

    def __repr__(self):
        """Object's representation."""

        return "Node ({}) {} -> [(L) {} / (R) {}]".format(
            id(self), self.item, str(self.lchild), str(self.rchild))


def btree_from_sorted_arr(arr):
    """Returns a balanced binary tree from a sorted array."""

    if len(arr) == 0:
        return None
    elif len(arr) == 1:
        return BinaryTreeNode(arr[0])

    midx = int(math.floor(len(arr) / 2.0))
    curr_node = BinaryTreeNode(arr[midx])
    curr_node.lchild = btree_from_sorted_arr(arr[:midx])
    curr_node.rchild = btree_from_sorted_arr(arr[midx + 1:])

    return curr_node


if __name__ == "__main__":
    """Entry point."""

    test_arr = [1, 1, 4, 6, 7, 13, 15]
    root_node = btree_from_sorted_arr(test_arr)

    print(">> Resulting binary tree")
    print("{}".format(root_node))
