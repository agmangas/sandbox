"""
Heaps implementation.
"""

import math


class BinaryMaxHeap(object):
    """Binary heap implementation."""

    def __init__(self):
        """Constructor."""

        self.heap = list()

    def __repr__(self):
        """Object's representation."""

        return "{}".format(self.heap)

    def _swap(self, idx1, idx2):
        """Swap the node values at idx1 and idx2."""

        self.heap[idx1], self.heap[idx2] = self.heap[idx2], self.heap[idx1]

    def is_empty(self):
        """Returns True if this heap is empty."""

        return True if not len(self.heap) else False

    def get_children(self, idx):
        """Returns the indexes of the left and right
        children of the heap element accessed by idx."""

        lchild_idx, rchild_idx = idx * 2 + 1, idx * 2 + 2

        if lchild_idx > len(self.heap) - 1:
            lchild_idx = None

        if rchild_idx > len(self.heap) - 1:
            rchild_idx = None

        return lchild_idx, rchild_idx

    @classmethod
    def get_parent(cls, idx):
        """Returns the parent index of the heap element
        accessed by idx."""

        parent_idx = int(math.floor((idx - 1) / 2.0))

        if parent_idx < 0:
            parent_idx = None

        return parent_idx

    def bubble_up(self, start_idx):
        """Reorganizes the tree defined by the node at start_idx and its
        direct children to maintain the heap property, then moves up the
        tree until it is no longer necessary."""

        lidx, ridx = self.get_children(start_idx)
        parent_idx = self.get_parent(start_idx)

        if lidx is not None and self.heap[lidx] > self.heap[start_idx]:
            self._swap(lidx, start_idx)

        if ridx is not None and self.heap[ridx] > self.heap[start_idx]:
            self._swap(ridx, start_idx)

        if parent_idx is not None and self.heap[start_idx] > self.heap[parent_idx]:
            self.bubble_up(parent_idx)

    def bubble_down(self, start_idx):
        """Reorganizes the tree defined by the node at start_idx and its
        direct children to maintain the heap property, then moves down the
        tree until it is no longer necessary."""

        lidx, ridx = self.get_children(start_idx)

        max_idx = max(
            [start_idx, lidx, ridx],
            key=lambda i: self.heap[i] if i is not None else None)

        if max_idx != start_idx:
            self._swap(max_idx, start_idx)
            self.bubble_down(max_idx)

    def insert(self, item):
        """Insert an item into the heap."""

        self.heap.append(item)
        self.bubble_up(len(self.heap) - 1)

    def pop(self):
        """Pops the max item from the heap."""

        root = self.heap[0]
        last_item = self.heap.pop()

        if not self.is_empty():
            self.heap[0] = last_item
            self.bubble_down(0)

        return root