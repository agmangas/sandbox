"""
Question extracted from: https://www.interviewcake.com/question/find-rotation-point?utm_source=weekly_email

I opened up a dictionary to a page in the middle and started flipping through, looking for words
I didn't know. I put each word I didn't know at increasing indices in a huge array I created in memory.
When I reached the end of the dictionary, I started from the beginning and did the same thing until I
reached the page I started at.

Now I have an array of words that are mostly alphabetical, except they start somewhere in the middle
of the alphabet, reach the end, and then start from the beginning of the alphabet. In other words,
this is an alphabetically ordered array that has been "rotated." For example:

  words = [
    'ptolemaic',
    'retrograde',
    'supplant',
    'undulate',
    'xenoepist',
    'asymptote', # <-- rotates here!
    'babka',
    'banoffee',
    'engender',
    'karpatka',
    'othellolagkage',
    ]

Write a function for finding the index of the "rotation point," which is where I started working from
the beginning of the dictionary. This array is huge (there are lots of words I don't know) so we want
to be efficient here.
"""

import pprint
import math


def find_rotation_point(words):
    """Finds the rotation point in a list of words sorted in two blocks."""

    get_pivot_idx = lambda l, r: int(math.floor((l + r) / 2.0))

    lidx, ridx = int(0), len(words) - 1
    iter_count, max_iter = int(0), math.ceil(math.log(len(words), 2))

    while True:
        if ridx - lidx <= 1:
            return ridx
        pivot_idx = get_pivot_idx(lidx, ridx)
        if words[lidx] <= words[pivot_idx]:
            lidx, ridx = pivot_idx, ridx
        else:
            lidx, ridx = lidx, pivot_idx
        iter_count += 1
        assert iter_count <= max_iter, "Too many iterations"


if __name__ == "__main__":
    """Entry point."""

    test_words = [
        'ptolemaic',
        'retrograde',
        'supplant',
        'undulate',
        'xenoepist',
        'asymptote',
        'babka',
        'banoffee',
        'engender',
        'karpatka',
        'othellolagkage'
    ]

    print(">> Test words list:")
    print("{}".format(pprint.pformat(test_words)))
    print(">> Rotation point: {}".format(test_words[find_rotation_point(test_words)]))
