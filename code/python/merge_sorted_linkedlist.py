"""
Merge two sorted linked lists into a single one without
using additional space.
"""


class LinkedListNode(object):
    """Simple linked list node."""

    def __init__(self, item):
        """Constructor."""

        self.item = item
        self.next = None

    def __repr__(self):
        """Object's representation."""

        return "{}".format(str(self.item))


def merge_sorted_lls(head_one, head_two):
    """Takes the head nodes of two sorted linked lists and merges them
    into one linked list without using additional space."""

    if head_one is None and head_two is None:
        return None
    elif head_one is None and head_two is not None:
        return head_two
    elif head_one is not None and head_two is None:
        return head_one

    if head_one.item <= head_two.item:
        next_node = head_one
        head_one_updated = head_one.next
        head_two_updated = head_two
    else:
        next_node = head_two
        head_one_updated = head_one
        head_two_updated = head_two.next

    next_node.next = merge_sorted_lls(head_one_updated, head_two_updated)

    return next_node


if __name__ == "__main__":
    """Entry point."""

    test_head_one = LinkedListNode(3)
    test_head_one.next = LinkedListNode(3)
    test_head_one.next.next = LinkedListNode(9)

    test_head_two = LinkedListNode(1)
    test_head_two.next = LinkedListNode(3)
    test_head_two.next.next = LinkedListNode(6)

    test_head_merged = merge_sorted_lls(test_head_one, test_head_two)
    test_node_pivot = test_head_merged

    while test_node_pivot is not None:
        print(">> Node: {}".format(test_node_pivot))
        test_node_pivot = test_node_pivot.next
