"""
Question extracted from: https://www.interviewcake.com/question/largest-stack

Use your Stack class to implement a new class MaxStack with a function get_max() that
returns the largest element in the stack. get_max() should not remove the item.

Your stacks will contain only integers.
"""

import random


class Stack:
    """Stack class implementation that should be used to implement a MaxStack class."""

    # initialize an empty list
    def __init__(self):
        self.items = []

    # push a new item to the last index
    def push(self, item):
        self.items.append(item)

    # remove the last item
    def pop(self):
        # if the stack is empty, return None
        # (it would also be reasonable to throw an exception)
        if not self.items:
            return None

        return self.items.pop()

    # see what the last item is
    def peek(self):
        # if the stack is empty, return None
        if not self.items:
            return None

        return self.items[len(self.items) - 1]


class MaxStack(object):
    """Stack that is able to return its max value."""

    def __init__(self):
        """Constructor."""

        self.stack = Stack()
        self.max_stack = Stack()

    def __len__(self):
        """Returns the object's length."""

        return len(self.stack.items)

    def push(self, item):
        """Append an item to the stack."""

        self.stack.push(item)

        if self.max_stack.peek() is None or item >= self.max_stack.peek():
            self.max_stack.push(item)

    def pop(self):
        """Pop an item from the stack."""

        popped = self.stack.pop()

        if self.max_stack.peek() == popped:
            self.max_stack.pop()

        return popped

    def peek(self):
        """See what the last item is."""

        return self.stack.peek()

    def get_max(self):
        """Returns the current max value. Doesn't pop it from the stack."""

        return self.max_stack.peek()

    def get_max_bf(self):
        """Returns the max obtained using a linear search."""

        try:
            return max(self.stack.items)
        except ValueError:
            return None


if __name__ == "__main__":
    """Entry point."""

    test_arrs = list()

    for dummy_one in xrange(10):
        test_arr = list()
        for dummy_two in xrange(random.randint(5, 20)):
            test_arr.append(random.randint(1, 10))
        test_arrs.append(test_arr)

    for arr in test_arrs:
        random.shuffle(arr)

    for test_arr in test_arrs:

        print(">> Testing stack: {}".format(test_arr))

        max_stack = MaxStack()
        curr_max = None

        for val in test_arr:
            if val > curr_max:
                curr_max = val
            max_stack.push(val)
            assert max_stack.get_max() == curr_max

        while len(max_stack):
            max_stack.pop()
            assert max_stack.get_max() == max_stack.get_max_bf()
