"""
Implementations and tests for linked list variations.
Usage: python linked_list.py
"""


class LinkedListNode(object):
    """Node of a linked list."""

    def __repr__(self):
        """Object's representation."""

        return "Node {}: {} -> {}".format(
            id(self), self.item, "None" if self.next_node is None else id(self.next_node))

    def __init__(self, item, next_node=None):
        """Constructor."""

        self.item = item
        self.next_node = next_node


class LinkedList(object):
    """Basic linked list implementation."""

    def __repr__(self):
        """Object's representation."""

        if self.head is None:
            return "Empty list"

        ret = ""

        curr_node = self.head

        while curr_node is not None:
            ret += str(curr_node) + "\n"
            curr_node = curr_node.next_node

        return ret.strip()

    def __init__(self):
        """Constructor."""

        self.head = None

    def insert(self, item):
        """Insert a new node."""

        if self.head is None:
            self.head = LinkedListNode(item)
            return

        curr_node = self.head

        while curr_node.next_node is not None:
            curr_node = curr_node.next_node

        curr_node.next_node = LinkedListNode(item)

    def search(self, item):
        """Search for a node with an item with the given value."""

        if self.head is None:
            return None

        curr_node = self.head

        while curr_node is not None:
            if curr_node.item == item:
                return curr_node
            curr_node = curr_node.next_node

        return None

    def search_predecessor(self, item):
        """Search for the predecessor of a node with an item with the given value."""

        if self.head is None:
            return None

        curr_node = self.head

        while curr_node.next_node is not None:
            if curr_node.next_node.item == item:
                return curr_node
            curr_node = curr_node.next_node

        return None

    def delete(self, item):
        """Delete node with the given value from the list."""

        node = self.search(item)
        assert node is not None

        predecessor = self.search_predecessor(item)

        # If the predecessor is None then this node is the head

        if predecessor is None:
            self.head = self.head.next_node
        else:
            predecessor.next_node = node.next_node


if __name__ == "__main__":
    """Do some tests with the linked list implementation."""

    linked_list = LinkedList()

    print(">> Empty linked list")
    print(linked_list)

    linked_list.insert(1)

    print(">> linked_list.insert(1)")
    print(linked_list)

    linked_list.insert(2)
    linked_list.insert(3)
    linked_list.insert(4)

    print(">> linked_list.insert(2)")
    print(">> linked_list.insert(3)")
    print(">> linked_list.insert(4)")
    print(linked_list)

    print(">> linked_list.search(1)")
    print(linked_list.search(1))

    print(">> linked_list.search(3)")
    print(linked_list.search(3))

    print(">> linked_list.search(9)")
    print(linked_list.search(9))

    linked_list.delete(2)

    print(">> linked_list.delete(2)")
    print(linked_list)

    linked_list.delete(1)

    print(">> linked_list.delete(1)")
    print(linked_list)

    linked_list.delete(4)

    print(">> linked_list.delete(4)")
    print(linked_list)

