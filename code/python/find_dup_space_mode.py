"""
Question extracted from: https://www.interviewcake.com/question/find-duplicate-optimize-for-space-beast-mode

In Find a duplicate, Space Edition, we were given an array of integers where:

    the integers are in the range 1..n1..n1..n
    the array has a length of n+1n+1n+1

These properties mean the array must have at least 1 duplicate. Our challenge was to find a
duplicate number, while optimizing for space. We used a divide and conquer approach, iteratively
cutting the array in half to find a duplicate integer in O(nlgn)O(n\lg{n})O(nlgn) time and O(1)
space (sort of a modified binary search).

But we can actually do better. We can find a duplicate integer in O(n)O(n)O(n) time while keeping
our space cost at O(1).
"""

import random


def find_duplicate_space_efficient(arr):
    """Takes an array of length n+1 with n integers from 1 to n and one unknown duplicate.
    Finds said duplicate."""

    node_nth_step = arr[-1]

    for idx in xrange(len(arr)):
        node_nth_step = arr[node_nth_step - 1]

    cycle_marker = node_nth_step
    cycle_curr_node = node_nth_step
    cycle_size = 0

    while True:
        cycle_size += 1
        cycle_curr_node = arr[cycle_curr_node - 1]
        if cycle_marker == cycle_curr_node:
            break

    cycle_ini_helper = arr[-1]
    cycle_ini = arr[-1]

    for idx in xrange(cycle_size):
        cycle_ini_helper = arr[cycle_ini_helper - 1]

    while True:
        if cycle_ini == cycle_ini_helper:
            break
        cycle_ini_helper = arr[cycle_ini_helper - 1]
        cycle_ini = arr[cycle_ini - 1]

    return cycle_ini


if __name__ == "__main__":
    """Entry point."""

    test_arrs = []

    for dummy in xrange(10):
        nsize = random.randint(2, 15)
        test_arr = [x + 1 for x in range(nsize)]
        test_arr_dup = random.randint(1, nsize)
        test_arr.append(test_arr_dup)
        random.shuffle(test_arr)
        test_arrs.append((test_arr, test_arr_dup))

    for test_arr, test_arr_dup in test_arrs:
        dup = find_duplicate_space_efficient(test_arr)
        print(">> Duplicate of {}: {}".format(test_arr, dup))
        assert dup == test_arr_dup