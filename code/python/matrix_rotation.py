"""
Rotate a matrix by 90 degrees.
"""

import math


def matrix_rotation(mat):
    """Rotates a matrix 90 degrees in place."""

    assert len(mat[0]) == len(mat), "Square matrix required"

    mat_len = len(mat[0])

    rotate_index = lambda idx: (idx[1], mat_len - idx[0] - 1)

    for i in range(0, int(math.floor(mat_len))):
        for j in range(i, mat_len - 1 - i):

            rot_list = list()
            pivot = (i, j)

            for k in range(4):
                rot_list.append(mat[pivot[0]][pivot[1]])
                pivot = rotate_index(pivot)

            rot_list.insert(0, rot_list.pop())

            for k in range(4):
                mat[pivot[0]][pivot[1]] = rot_list[k]
                pivot = rotate_index(pivot)

    return mat


if __name__ == "__main__":
    """Entry point."""

    test_mat_one = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ]

    test_mat_two = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]

    print(">> Original matrix: {}".format(test_mat_one))
    test_mat_one_rot = matrix_rotation(test_mat_one)
    print(">> Rotated test matrix: {}".format(test_mat_one_rot))

    print(">> Original matrix: {}".format(test_mat_two))
    test_mat_two_rot = matrix_rotation(test_mat_two)
    print(">> Rotated test matrix: {}".format(test_mat_two_rot))