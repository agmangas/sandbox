"""
Some stack-related problems.
"""


def peek(stack):
    """Peek a stack."""

    try:
        return stack[-1]
    except IndexError:
        return None


def get_stock_span(stock_values):
    """The stock span problem is a financial problem where we have a series of n daily price
    quotes for a stock and we need to calculate span of stock's price for all n days.
    The span Si of the stock's price on a given day i is defined as the maximum number of
    consecutive days just before the given day, for which the price of the stock on the
    current day is less than or equal to its price on the given day.
    For example, if an array of 7 days prices is given as {100, 80, 60, 70, 60, 75, 85},
    then the span values for corresponding 7 days are {1, 1, 1, 2, 1, 4, 6}."""

    stack, ret = list(), list()

    for idx, val in enumerate(stock_values):
        while peek(stack) is not None and peek(stack)[1] < val:
            stack.pop()
        if peek(stack) is not None:
            ret.append(idx - peek(stack)[0])
        else:
            ret.append(1)
        stack.append((idx, val))

    return ret


def next_greater_element(arr):
    """Given an array, print the Next Greater Element (NGE) for every element. The Next greater
    Element for an element x is the first greater element on the right side of x in array.
    Elements for which no greater element exist, consider next greater element as -1."""

    stack, ret = list(), [-1] * len(arr)

    for idx, val in enumerate(arr):
        while peek(stack) is not None and peek(stack)[1] < val:
            popped = stack.pop()
            ret[popped[0]] = val
        stack.append((idx, val))

    return ret


if __name__ == "__main__":
    """Entry point."""

    test_arr = [100, 80, 60, 70, 60, 75, 85, 120]

    test_arr_stock_span = [1, 1, 1, 2, 1, 4, 6, 1]
    stock_span = get_stock_span(test_arr)
    print(">> (Stock span) {}: {}".format(test_arr, stock_span))
    assert stock_span == test_arr_stock_span

    test_arr_next_greater = [120, 85, 70, 75, 75, 85, 120, -1]
    next_greater = next_greater_element(test_arr)
    print(">> (Next greater) {}: {}".format(test_arr, next_greater))
    assert next_greater == test_arr_next_greater