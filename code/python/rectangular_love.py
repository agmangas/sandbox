"""
Question extracted from: https://www.interviewcake.com/question/rectangular-love

A crack team of love scientists from OkEros (a hot new dating site) have devised a way
to represent dating profiles as rectangles on a two-dimensional plane.

They need help writing an algorithm to find the intersection of two users' love rectangles.
They suspect finding that intersection is the key to a matching algorithm so powerful it will
cause an immediate acquisition by Google or Facebook or Obama or something.

Write a function to find the rectangular intersection of two given love rectangles.

As with the example above, love rectangles are always "straight" and never "diagonal."
More rigorously: each side is parallel with either the x-axis or the y-axis.

They are defined as hash maps like this:

my_rectangle = {

    # coordinates of bottom-left corner:
    'x': 1,
    'y': 5,

    # width and height
    'width': 10,
    'height': 4,

    }
"""


def rect_intersection(rect1, rect2):
    """Returns the rectange that is the intersection of the two arguments."""

    ret = {"x": None, "y": None, "width": None, "height": None}

    for ra, rb in [(rect1, rect2), (rect2, rect1)]:

        lft_inters = rb["x"] <= ra["x"] <= (rb["x"] + rb["width"])
        rgt_inters = rb["x"] <= (ra["x"] + ra["width"]) <= (rb["x"] + rb["width"])
        btm_inters = rb["y"] <= ra["y"] <= (rb["y"] + rb["height"])
        top_inters = rb["y"] <= (ra["y"] + ra["height"]) <= (rb["y"] + rb["height"])

        if True in [lft_inters, rgt_inters, top_inters, btm_inters]:

            if lft_inters:
                ret["x"] = ra["x"]
                ret["width"] = ra["width"] if rgt_inters else (rb["x"] + rb["width"]) - ra["x"]
            elif rgt_inters:
                ret["x"] = rb["x"]
                ret["width"] = (ra["x"] + ra["width"]) - rb["x"]
            else:
                ret["x"] = rb["x"]
                ret["width"] = rb["width"]

            if btm_inters:
                ret["y"] = ra["y"]
                ret["height"] = ra["height"] if top_inters else (rb["y"] + rb["height"]) - ra["y"]
            elif top_inters:
                ret["y"] = rb["y"]
                ret["height"] = (ra["y"] + ra["height"]) - rb["y"]
            else:
                ret["y"] = rb["y"]
                ret["height"] = rb["height"]

    assert not None in ret.values(), "No intersection found"

    return ret


if __name__ == "__main__":
    """Entry point."""

    test_rect1 = {
        "x": 1,
        "y": 5,
        "width": 10,
        "height": 4
    }

    test_rect2 = {
        "x": 3,
        "y": 2,
        "width": 5,
        "height": 8
    }

    test_intersect_12 = rect_intersection(test_rect1, test_rect2)
    print(">> Intersection of {} and {}:".format(test_rect1, test_rect2))
    print("{}".format(test_intersect_12))
    assert test_intersect_12 == {"x": 3, "y": 5, "width": 5, "height": 4}

    test_rect3 = {
        "x": 2,
        "y": 2,
        "width": 200,
        "height": 200
    }

    test_rect4 = {
        "x": test_rect3["x"] + int(test_rect3["width"] / 2),
        "y": test_rect3["y"] + int(test_rect3["height"] / 2),
        "width": int(test_rect3["width"] / 4.0),
        "height": int(test_rect3["height"] / 4.0)
    }

    test_intersect_34 = rect_intersection(test_rect3, test_rect4)
    print(">> Intersection of {} and {}:".format(test_rect3, test_rect4))
    print("{}".format(test_intersect_34))
    assert test_intersect_34 == test_rect4

    test_rect5 = {"x": 0, "y": 0, "width": 1, "height": 1}
    test_rect6 = {"x": 5, "y": 5, "width": 2, "height": 2}

    try:
        rect_intersection(test_rect5, test_rect6)
        raise Exception("{} and {} should not intersect!".format(test_rect5, test_rect6))
    except AssertionError:
        print(">> {} and {} do not intersect".format(test_rect5, test_rect6))
