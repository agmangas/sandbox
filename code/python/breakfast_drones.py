"""
Question extracted from: https://www.interviewcake.com/question/find-unique-int-among-duplicates

Your company delivers breakfast via autonomous quadcopter drones. And something mysterious has happened.

Each breakfast delivery is assigned a unique ID, a positive integer. When one of the company's 100 drones
takes off with a delivery, the delivery's ID is added to an array, delivery_id_confirmations. When the
drone comes back and lands, the ID is again added to the same array.

After breakfast this morning there were only 99 drones on the tarmac. One of the drones never made it back
from a delivery. We suspect a secret agent from Amazon placed an order and stole one of our patented drones.
To track them down, we need to find their delivery ID.

Given the array of IDs, which contains many duplicate integers and one unique integer, find the unique integer.

The IDs are not guaranteed to be sorted or sequential. Orders aren't always fulfilled in the order they were
received, and some deliveries get cancelled before takeoff.
"""

import pprint


def find_unique_integer(arr):
    """Finds the unique integer in an array of integers."""

    found, candidates = dict(), dict()

    for val in arr:
        if not val in found:
            found[val] = 1
            candidates[val] = None
        else:
            found[val] += 1
            if val in candidates:
                del candidates[val]

    assert len(candidates.keys()) == 1, "More than one unique value"

    return candidates.keys()[0]


if __name__ == "__main__":
    """Entry point."""

    delivery_id_confirmations = [
        10, 1, 34, 3, 5, 71, 6, 8, 11, 9,
        1, 10, 8, 3, 9, 34, 6, 5, 71, 142, 11
    ]

    unique_int = find_unique_integer(delivery_id_confirmations)

    print(">> Unique integer in {}: {}".format(
        pprint.pformat(delivery_id_confirmations), unique_int))