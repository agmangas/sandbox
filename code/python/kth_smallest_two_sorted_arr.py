"""
Find Kth smallest element in two sorted arrays
For example if A = [10, 20, 40, 60] and B = [15, 35, 50, 70, 100] and K = 4 then the solution should be 35
because the union of the previous arrays would be C = [10, 15, 20, 35, 40, 50, 60, 70, 100] and the fourth
smallest element of this union is 35.
"""

import math


def find_kth_smallest(arr1, arr2, k):
    """Takes two sorted arrays and returns the kth smallest element in both of them."""

    assert len(arr1) + len(arr2) >= k

    if len(arr1) == 0:
        return arr2[k - 1]
    elif len(arr2) == 0:
        return arr1[k - 1]
    elif k == 1:
        return arr1[0] if arr1[0] < arr2[0] else arr2[0]

    k_half_ceil = int(math.ceil(k / 2.0))
    pivot_arr1 = (k_half_ceil - 1) if len(arr1) >= k_half_ceil else len(arr1) - 1
    pivot_arr2 = k - pivot_arr1 - 2

    assert pivot_arr1 + pivot_arr2 == (k - 2)

    sub_arr1, sub_arr2 = arr1, arr2

    if arr1[pivot_arr1] > arr2[pivot_arr2]:
        sub_arr1 = sub_arr1[:pivot_arr1 + 1]
        sub_arr2 = sub_arr2[pivot_arr2 + 1:]
        k_sub = pivot_arr1 + 1
    else:
        sub_arr1 = sub_arr1[pivot_arr1 + 1:]
        sub_arr2 = sub_arr2[:pivot_arr2 + 1]
        k_sub = pivot_arr2 + 1

    return find_kth_smallest(sub_arr1, sub_arr2, k_sub)


if __name__ == "__main__":
    """Entry point."""

    assert find_kth_smallest(
        [1, 3, 5, 7, 9, 11],
        [0, 2, 4, 6, 8], 6) == 5

    assert find_kth_smallest(
        [1, 3, 5, 7, 9, 11],
        [0, 2, 4, 6, 8], 7) == 6

    assert find_kth_smallest(
        [1, 1, 5, 7, 9, 11],
        [0, 1, 4, 6, 8], 5) == 4

    assert find_kth_smallest(
        [1, 1, 5, 7, 9, 11],
        [0, 1, 4, 6, 8], 3) == 1

    assert find_kth_smallest(
        [1, 1, 5, 7, 9, 11],
        [0, 1, 4, 6, 8], 11) == 11