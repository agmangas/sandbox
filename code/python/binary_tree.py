"""
Binary trees related code.
"""

import random


class BinaryTreeNode(object):
    """Represents a binary tree node."""

    def __init__(self, item):
        """Constructor."""

        self.item = item
        self.rchild = None
        self.lchild = None

    def __repr__(self):
        """Object's representation."""

        return "Node {}: {} (LChild: {}, RChild: {})".format(
            id(self), self.item,
            id(self.lchild) if self.lchild else None,
            id(self.rchild) if self.rchild else None)

    def has_children(self):
        """Returns True if this node has children."""

        return True if self.lchild is not None or self.rchild is not None else False

    def iterate_inorder(self):
        """In-order traversal from this node."""

        if self.lchild is not None:
            for lnode in self.lchild.iterate_inorder():
                yield lnode

        yield self

        if self.rchild is not None:
            for rnode in self.rchild.iterate_inorder():
                yield rnode

    def iterate_preorder(self):
        """Pre-order traversal from this node."""

        yield self

        if self.lchild is not None:
            for lnode in self.lchild.iterate_inorder():
                yield lnode

        if self.rchild is not None:
            for rnode in self.rchild.iterate_inorder():
                yield rnode

    def iterate_postorder(self):
        """Post-order traversal from this node."""

        if self.lchild is not None:
            for lnode in self.lchild.iterate_inorder():
                yield lnode

        if self.rchild is not None:
            for rnode in self.rchild.iterate_inorder():
                yield rnode

        yield self

    def deletion_subtree(self):
        """Returns the subtree that must be linked by the parent when
        this node is deleted from the tree."""

        if not self.has_children():
            return None
        elif self.has_children() and None in [self.lchild, self.rchild]:
            return self.lchild if self.lchild is not None else self.rchild

        successor_parent = self
        successor = self.rchild

        while successor.lchild is not None:
            successor_parent = successor
            successor = successor.lchild

        self.item = successor.item

        if successor == self.rchild:
            successor_parent.rchild = successor.deletion_subtree()
        else:
            successor_parent.lchild = successor.deletion_subtree()

        return self


class BinaryTree(object):
    """Represents a binary tree."""

    def __init__(self, root_item):
        """Constructor."""

        self.root = BinaryTreeNode(root_item)

    def insert(self, val):
        """Insert a new item into the tree."""

        tree_node = BinaryTreeNode(val)

        curr_node, stop = self.root, False

        while not stop:
            if val > curr_node.item:
                if curr_node.rchild is None:
                    curr_node.rchild = tree_node
                    stop = True
                else:
                    curr_node = curr_node.rchild
            else:
                if curr_node.lchild is None:
                    curr_node.lchild = tree_node
                    stop = True
                else:
                    curr_node = curr_node.lchild

    def iterate_inorder(self):
        """In-order iterator.
        In a binary tree this generates a sorted list of values."""

        return self.root.iterate_inorder()

    def iterate_preorder(self):
        """Pre-order iterator."""

        return self.root.iterate_preorder()

    def iterate_postorder(self):
        """Post-order iterator."""

        return self.root.iterate_postorder()

    def search(self, item):
        """Returns the BinaryTreeNode that contains the given item."""

        if self.root is None:
            raise ValueError("Not found")

        curr_node = self.root

        while curr_node is not None:
            if item > curr_node.item:
                curr_node = curr_node.rchild
            elif item < curr_node.item:
                curr_node = curr_node.lchild
            else:
                return curr_node

        raise ValueError("Not found")

    def search_parent(self, item):
        """Returns the BinaryTreeNode that is the parent of the node
        that contains the given item."""

        if self.root is None or not self.root.has_children():
            raise ValueError("Not Found")

        curr_node = self.root

        while curr_node is not None:
            lchild_match = curr_node.lchild is not None and curr_node.lchild.item == item
            rchild_match = curr_node.rchild is not None and curr_node.rchild.item == item

            if lchild_match or rchild_match:
                return curr_node

            if item > curr_node.item:
                curr_node = curr_node.rchild
            elif item < curr_node.item:
                curr_node = curr_node.lchild

        raise ValueError("Not found")

    def delete(self, item):
        """Deletes the node with the given item from this tree."""

        searched_node = self.search(item)

        if searched_node is not self.root:
            searched_node_parent = self.search_parent(searched_node.item)
            if searched_node_parent.lchild is searched_node:
                searched_node_parent.lchild = searched_node.deletion_subtree()
            else:
                searched_node_parent.rchild = searched_node.deletion_subtree()
        else:
            self.root = self.root.deletion_subtree()


if __name__ == "__main__":
    """Entry point."""

    btree_items = [10, 40, 1, 3, 48, 7, 85, 28, 2, 2, 11, 10]

    btree = BinaryTree(btree_items[0])

    for bitem in btree_items[1:]:
        btree.insert(bitem)

    print(">> In-order traversal")

    for node in btree.iterate_inorder():
        print node

    print(">> Pre-order traversal")

    for node in btree.iterate_preorder():
        print node

    print(">> Post-order traversal")

    for node in btree.iterate_postorder():
        print node

    print(">> Delete some values")

    shuffled_btree_items = btree_items[:]
    random.shuffle(shuffled_btree_items)

    for shuffled_val in shuffled_btree_items[:-1]:
        print(">> Deleting value: {}".format(shuffled_val))
        btree.delete(shuffled_val)
        inorder_items = [bnode.item for bnode in btree.iterate_inorder()]
        print(">> Asserting that the in order items are sorted")
        assert inorder_items == sorted(inorder_items)