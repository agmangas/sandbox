"""
Recursion and dynamic programming problems.
"""


# noinspection PyPep8Naming
def longest_common_subseq(text, pattern, cache=None):
    """Extract from Skiena's Algorithms Manual:
    Longest Common Subsequence - Perhaps we are interested in finding the
    longest scattered string of characters included within both strings. Indeed,
    this problem will be discussed in Section 18.8. The longest common subsequence
    (LCS) between 'democrat' and 'republican' is 'eca'."""

    END_MATCH = "end_match"
    TEXT_REDUCE = "text_reduce"
    PATTERN_REDUCE = "pattern_reduce"

    cache = dict() if cache is None else cache

    if (text, pattern) in cache:
        return cache[(text, pattern)]

    if not len(text) or not len(pattern):
        return ""

    sub_res = {
        END_MATCH: longest_common_subseq(text[:-1], pattern[:-1], cache=cache),
        TEXT_REDUCE: longest_common_subseq(text[:-1], pattern, cache=cache),
        PATTERN_REDUCE: longest_common_subseq(text, pattern[:-1], cache=cache)
    }

    if text[-1] == pattern[-1]:
        sub_res[END_MATCH] += text[-1]

    best_sub = END_MATCH

    for key in set(sub_res.keys()) - {best_sub}:
        if len(sub_res[key]) > len(sub_res[best_sub]):
            best_sub = key

    cache[(text, pattern)] = sub_res[best_sub]

    return sub_res[best_sub]


def longest_increasing_seq(sequence, cache=None):
    """Extract from Skiena's Algorithms Manual:
    A numerical sequence is monotonically increasing if the ith element is at
    least as big as the (i-1)st element. The maximum monotone subsequence problem
    seeks to delete the fewest number of elements from an input string S to leave
    a monotonically increasing subsequence. A longest increasing subsequence of
    243517698 is 23568."""

    cache = dict() if cache is None else cache

    if len(sequence) == 1:
        return sequence

    if tuple(sequence) in cache:
        return cache[tuple(sequence)]

    sub_results = [
        longest_increasing_seq(sequence[:idx + 1], cache=cache)
        for idx in xrange(len(sequence) - 1)
    ]

    sub_results = [
        sub_result for sub_result in sub_results
        if sub_result[-1] < sequence[-1]
    ]

    if not len(sub_results):
        return [sequence[-1]]

    sub_results_len = [len(subseq) for subseq in sub_results]
    best_result_idx = sub_results_len.index(max(sub_results_len))
    result = sub_results[best_result_idx] + [sequence[-1]]

    assert sorted(result) == result

    cache[tuple(sequence)] = result

    return result


def _parts_cost(sequence, divisions):
    """Returns the total cost of the sequence partitioned by the division list."""

    assert len(divisions) > 0, "At least two partitions needed"

    subseqs = list()

    subseqs.append(sequence[:divisions[0] + 1])

    for idx in range(1, len(divisions) - 1):
        lft_pivot = divisions[idx - 1] + 1
        rgt_pivot = divisions[idx] + 1
        subseqs.append(sequence[lft_pivot:rgt_pivot])

    subseqs.append(sequence[divisions[-1] + 1:])

    subseq_sum = [sum(subseq) for subseq in subseqs]
    cost = max(subseq_sum) - min(subseq_sum)

    return cost


def partition_seq(seqnc, k):
    """Create a division of k partitions with the same approximate weight
    for each division in a sequence of numbers.
    Example: 100 200 300 400 500 | 600 700 | 800 900."""

    assert k > 1

    parts_mat = list()

    for i in range(len(seqnc)):
        parts_mat.append([None] * k)

    for j in range(1, k):
        parts_mat[j][j] = tuple(range(j))

    for j in range(1, k):
        for i in range(j + 1, len(seqnc)):
            if j > 1:
                prev_parts = [
                    tuple(list(parts_mat[idx][j - 1]) + [idx])
                    for idx in range(j - 1, i)
                ]
                parts_costs = [
                    (part, _parts_cost(seqnc[:i + 1], part))
                    for part in prev_parts
                ]
            else:
                parts = range(1, i)
                parts_costs = [
                    (tuple([part]), _parts_cost(seqnc[:i + 1], tuple([part])))
                    for part in parts
                ]

            min_part = min(parts_costs, key=lambda t: t[1])[0]
            parts_mat[i][j] = min_part

    return parts_mat[-1][-1]


def max_subarr(arr):
    """Returns the subarray of the given array that generates the maximum sum."""

    if len(arr) == 1:
        return arr

    subarr_result = max_subarr(arr[:-1])
    solutions = [(subarr_result, sum(subarr_result))]
    for idx in xrange(len(arr)):
        solutions.append((arr[idx:], sum(arr[idx:])))
    max_solution = max(solutions, key=lambda t: t[1])

    return max_solution[0]


def max_subarr_not_consecutive(arr):
    """Returns the subarray of the given array that generates the maximum sum.
    The members of the subarray cannot appear consecutively in the original array."""

    max_sum = list()

    max_sum.append(0)
    max_sum.append(arr[0] if arr[0] > 0 else 0)

    for idx in range(2, len(arr) + 1):
        element_if_positive = arr[idx - 1] if arr[idx - 1] > 0 else 0
        new_val = max((max_sum[idx - 2] + element_if_positive, max_sum[idx - 1]))
        max_sum.append(new_val)

    return max_sum[-1]


def stolen_houses(arr):
    """Problem: There are n houses built in a line, each of which contains some value in it.
    A thief is going to steal the maximal value in these houses, but he cannot steal in two
    adjacent houses because the owner of a stolen house will tell his two neighbors on the
    left and right side. What is the maximal stolen value?
    For example, if there are four houses with values {6, 1, 2, 7}, the maximal stolen value
    is 13 when the first and fourth houses are stolen."""

    if len(arr) == 1:
        return arr[0]
    elif len(arr) == 2:
        return max(arr)

    sol_arr = [arr[0], max((arr[0], arr[1]))]

    for idx in range(2, len(arr)):
        this_solution = max((arr[idx] + sol_arr[idx - 2], sol_arr[idx - 1]))
        sol_arr.append(this_solution)

    return sol_arr[len(arr) - 1]


def edit_distance(str_a, str_b):
    """Returns the edit distance from str_a to str_b."""

    dmat = list()

    for i in range(len(str_b) + 1):
        dmat.append([None] * (len(str_a) + 1))

    for i in range(len(str_b) + 1):
        for j in range(len(str_a) + 1):
            if i == 0:
                val = j
            elif j == 0:
                val = i
            else:
                equal_factor = 0 if str_a[j - 1] == str_b[i - 1] else 1
                val = min([
                    dmat[i - 1][j - 1] + equal_factor,
                    dmat[i - 1][j] + 1,
                    dmat[i][j - 1] + 1
                ])
            dmat[i][j] = val

    assert len(dmat) == len(str_b) + 1
    assert len(dmat[0]) == len(str_a) + 1

    return dmat[len(str_b)][len(str_a)]


if __name__ == "__main__":
    """Entry point."""

    str_pairs = [
        ("democrat", "republican"),
        ("tania", "andres"),
        ("castlevania", "casterly")
    ]

    for stra, strb in str_pairs:
        print(">> Longest common subsequence ('{}' and '{}'): '{}'".format(
            stra, strb, longest_common_subseq(stra, strb)))

    sequences = [
        [2, 4, 3, 5, 1, 7, 6, 9, 8],
        [2, 4, 3, 5, 1, 7, 6, 9, 8, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [2, 1, 9, 3, 4, 2, 1, 9, 8, 7, 4, 5, 6]
    ]

    for seq in sequences:
        print(">> Longest increasing sequence ({}): {}".format(
            seq, longest_increasing_seq(seq)))

    sequences_div = [
        ([100, 100, 100], 3),
        ([100, 100, 100, 100, 100, 100], 3),
        ([100, 200, 300, 400, 500, 600, 700, 800, 900], 3),
        ([100, 200, 300, 400, 500, 600, 700, 800, 900], 4),
        ([100, 200, 300, 400, 500, 600, 700, 800, 900], 5)
    ]

    for sequence_div in sequences_div:
        print(">> Sequence partition ({}): {}".format(
            sequence_div, partition_seq(*sequence_div)))

    test_arrs = [
        [4, 5, -1, -7, 3, -1],
        [1, 2, 3, 4, 5, 6],
        [-2, 1, -3, 4, -1, 2, 1, -5, 4],
        [6, 1, 2, 7]
    ]

    for test_arr in test_arrs:
        print(">> ** Testing array: {}".format(test_arr))
        print(">> Max sum subarray: {}".format(max_subarr(test_arr)))
        print(">> Max sum subarray (not consec.): {}".format(
            max_subarr_not_consecutive(test_arr)))
        print(">> Max stolen value: {}".format(stolen_houses(test_arr)))

    strs_edit_test = [
        ("tania", "tanya"),
        ("andres", "andres"),
        ("cthulhu", "catulu"),
        ("a", "abcdfg")
    ]

    for str_edit_test in strs_edit_test:
        print(">> Edit distance from '{}' to '{}': {}".format(
            str_edit_test[0], str_edit_test[1], edit_distance(*str_edit_test)))