"""
Find the largest difference in an array.
"""

import random


def largest_diff(arr):
    """Returns the largest difference between elements in the array.
    The smaller element must occur earlier in the array."""

    tmp = list()

    def get_min_idx():
        min_values = [p[0] for p in tmp]
        return min_values.index(min(min_values))

    def is_smaller_min(candidate):
        if len(tmp):
            current_min = min(p[0] for p in tmp)
            return candidate < current_min
        else:
            return True

    def is_larger_max(candidate):
        if len(tmp):
            min_pair = tmp[get_min_idx()]
            return candidate > min_pair[1] if min_pair[1] is not None else True
        else:
            return True

    for val in arr:
        if is_smaller_min(val):
            tmp.append([val, None])
        elif is_larger_max(val):
            tmp[get_min_idx()][1] = val

    tmp = [pair for pair in tmp if pair[1] is not None]

    if not len(tmp):
        raise ValueError("The input array is in strict ascending order")

    diffs = [pair[1] - pair[0] for pair in tmp]

    return max(diffs)


if __name__ == "__main__":
    test_arrs = list()

    for dummy_idx in range(5):
        new_arr = list()
        for dummy_idx_2 in range(5):
            new_arr.append(random.randint(1, 40))
        test_arrs.append(new_arr)

    for test_arr in test_arrs:
        test_arr_diff = largest_diff(test_arr)
        print(">> Largest diff of {}: {}".format(test_arr, test_arr_diff))