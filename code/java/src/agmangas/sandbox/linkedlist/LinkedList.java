package agmangas.sandbox.linkedlist;

public class LinkedList<T> {
    private LinkedListNode head;

    public LinkedList() {
        this.head = null;
    }

    public String toString() {
        if (head == null) {
            return String.format("Empty list %n");
        } else {
            String ret = "";
            LinkedListNode currNode = head;
            while (currNode != null) {
                ret = ret + String.format(
                        "Node %s (%s) -> %s%n", System.identityHashCode(currNode),
                        currNode.getItem(), System.identityHashCode(currNode.getNext()));
                currNode = currNode.getNext();
            }
            return ret;
        }
    }

    public void insert(T item) {
        if (head == null) {
            head = new LinkedListNode<T>(item);
        } else {
            LinkedListNode lastNode = head;
            while (lastNode.getNext() != null) {
                lastNode = lastNode.getNext();
            }
            lastNode.setNext(new LinkedListNode<T>(item));
        }
    }

    public LinkedListNode search(T item) throws NodeNotFoundException {
        LinkedListNode currNode = head;

        while (currNode != null) {
            if (currNode.getItem() == item) {
                return currNode;
            } else {
                currNode = currNode.getNext();
            }
        }

        throw new NodeNotFoundException();
    }

    public LinkedListNode searchPredecessor(T item) throws NodeNotFoundException {
        LinkedListNode currNode = head;

        while (currNode.getNext() != null) {
            if (currNode.getNext().getItem() == item) {
                return currNode;
            } else {
                currNode = currNode.getNext();
            }
        }

        throw new NodeNotFoundException();
    }

    public void delete(T item) throws NodeNotFoundException {
        LinkedListNode delNode = this.search(item);
        if (delNode == head) {
            head = head.getNext();
        } else {
            LinkedListNode predNode = this.searchPredecessor(item);
            predNode.setNext(delNode.getNext());
        }
    }
}
