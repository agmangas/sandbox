package agmangas.sandbox.linkedlist;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> testLinkedList = new LinkedList<Integer>();

        System.out.println(testLinkedList);

        testLinkedList.insert(1);

        System.out.println(testLinkedList);

        testLinkedList.insert(2);
        testLinkedList.insert(3);
        testLinkedList.insert(4);

        System.out.println(testLinkedList);

        try {
            LinkedListNode found = testLinkedList.search(1);
            System.out.println(found);
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        try {
            testLinkedList.delete(3);
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(testLinkedList);

        try {
            testLinkedList.delete(1);
        } catch (NodeNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(testLinkedList);
    }
}
