package agmangas.sandbox.linkedlist;

public class LinkedListNode<T> {
    private T item;
    private LinkedListNode next;

    public LinkedListNode(T item) {
        this.item = item;
        this.next = null;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public LinkedListNode getNext() {
        return next;
    }

    public void setNext(LinkedListNode next) {
        this.next = next;
    }
}
