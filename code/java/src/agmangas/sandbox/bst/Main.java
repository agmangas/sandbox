package agmangas.sandbox.bst;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Integer rootValue = 6;
        int[] nodeValues = {7, 1, 4, 53, 12, 4, 2, 5, 9, 17};

        BinaryTree binaryTree = new BinaryTree();
        BinaryTreeNode rootNode = new BinaryTreeNode(rootValue);
        binaryTree.setRoot(rootNode);

        for (int i = 0; i < nodeValues.length; i++) {
            binaryTree.insert(new BinaryTreeNode(nodeValues[i]));
        }

        System.out.println(String.format(">> Binary Tree:%n%s", binaryTree));
        System.out.println(">> Doing some searches");

        BinaryTreeNode node;

        try {
            node = binaryTree.search(nodeValues[0]);
            System.out.println(node);
        } catch (BinaryTreeNodeNotFound e) {

        }

        try {
            node = binaryTree.search(nodeValues[1]);
            System.out.println(node);
        } catch (BinaryTreeNodeNotFound e) {

        }

        try {
            System.out.println(">> Searching for inexistent item");
            binaryTree.search(10e10);
        } catch (BinaryTreeNodeNotFound e) {
            System.out.println(">> Not Found: OK");
        }

        try {
            int randomValue = nodeValues[new Random().nextInt(nodeValues.length)];
            System.out.println(">> Deleting node: " + randomValue);
            binaryTree.delete(randomValue);
        } catch (BinaryTreeNodeNotFound e) {

        }

        System.out.println(String.format(">> Binary Tree:%n%s", binaryTree));

        try {
            System.out.println(">> Deleting root: " + rootValue);
            binaryTree.delete(rootValue);
        } catch (BinaryTreeNodeNotFound e) {

        }

        System.out.println(String.format(">> Binary Tree:%n%s", binaryTree));
    }
}
