package agmangas.sandbox.bst;

import java.util.List;

public class BinaryTree {
    private BinaryTreeNode root;

    public BinaryTree() {
        this.root = null;
    }

    public String toString() {
        String ret = "";

        List<BinaryTreeNode> nodesInOrder = this.traverseInOrder();
        for (BinaryTreeNode node : nodesInOrder) {
            ret += String.format("[Node] %s%n", node.toString());
        }

        return ret;
    }

    public BinaryTreeNode getRoot() {
        return root;
    }

    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }

    public void insert(BinaryTreeNode newNode) {
        this.getRoot().insert(newNode);
    }

    public List<BinaryTreeNode> traverseInOrder() {
        return this.getRoot().traverseInOrder();
    }

    public BinaryTreeNode search(Number item) throws BinaryTreeNodeNotFound {
        BinaryTreeNode currNode = this.getRoot();
        boolean isLower;

        while (currNode != null) {
            if (currNode.getItem().equals(item))
                return currNode;

            isLower = item.doubleValue() <= currNode.getItem().doubleValue();

            if (currNode.getLchild() != null && isLower) {
                currNode = currNode.getLchild();
            } else if (currNode.getRchild() != null && !isLower) {
                currNode = currNode.getRchild();
            } else {
                currNode = null;
            }
        }

        throw new BinaryTreeNodeNotFound();
    }

    public BinaryTreeNode searchParent(Number item) throws BinaryTreeNodeNotFound {
        BinaryTreeNode currNode = this.getRoot();
        boolean isLparent;
        boolean isRparent;
        boolean isLower;

        while (currNode != null) {
            isLparent = currNode.getLchild() != null && currNode.getLchild().getItem().equals(item);
            isRparent = currNode.getRchild() != null && currNode.getRchild().getItem().equals(item);

            if (isLparent || isRparent)
                return currNode;

            isLower = item.doubleValue() <= currNode.getItem().doubleValue();

            if (currNode.getLchild() != null && isLower) {
                currNode = currNode.getLchild();
            } else if (currNode.getRchild() != null && !isLower) {
                currNode = currNode.getRchild();
            } else {
                currNode = null;
            }
        }

        throw new BinaryTreeNodeNotFound();
    }

    public void delete(Number item) throws BinaryTreeNodeNotFound {
        BinaryTreeNode node = this.search(item);

        if (this.getRoot().equals(node)) {
            if (!node.hasChildren()) {
                this.setRoot(null);
            } else if (node.hasOnlyOneChild()) {
                this.setRoot(node.getFirstChild());
            } else {
                node.substituteBySuccessor();
            }
            return;
        }

        BinaryTreeNode parentNode = this.searchParent(item);

        if (!node.hasChildren()) {
            if (parentNode.isLchild(node))
                parentNode.setLchild(null);
            else
                parentNode.setRchild(null);
        } else if (node.hasOnlyOneChild()) {
            if (parentNode.isLchild(node))
                parentNode.setLchild(node.getFirstChild());
            else
                parentNode.setRchild(node.getFirstChild());
        } else {
            node.substituteBySuccessor();
        }
    }
}
