package agmangas.sandbox.bst;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeNode {
    private Number item;
    private BinaryTreeNode lchild;
    private BinaryTreeNode rchild;

    public BinaryTreeNode(Number item) {
        this.item = item;
        this.lchild = null;
        this.rchild = null;
    }

    public String toString() {
        String lchildRepr = "L:NULL";
        String rchildRepr = "R:NULL";

        if (this.getLchild() != null)
            lchildRepr = String.format("L:%s", this.getLchild().getItem());

        if (this.getRchild() != null)
            rchildRepr = String.format("R:%s", this.getRchild().getItem());

        return String.format(
                "Item:%s -> %s / %s", this.getItem(), lchildRepr, rchildRepr);
    }

    public Number getItem() {
        return item;
    }

    public void setItem(Number item) {
        this.item = item;
    }

    public BinaryTreeNode getLchild() {
        return lchild;
    }

    public void setLchild(BinaryTreeNode lchild) {
        this.lchild = lchild;
    }

    public boolean hasLchild() {
        return this.getLchild() != null;
    }

    public boolean isLchild(BinaryTreeNode node) {
        return this.hasLchild() && this.getLchild().equals(node);
    }

    public BinaryTreeNode getRchild() {
        return rchild;
    }

    public void setRchild(BinaryTreeNode rchild) {
        this.rchild = rchild;
    }

    public boolean hasRchild() {
        return this.getRchild() != null;
    }

    public boolean isRchild(BinaryTreeNode node) {
        return this.hasRchild() && this.getRchild().equals(node);
    }

    public boolean hasChildren() {
        return this.hasLchild() || this.hasRchild();
    }

    public boolean hasOnlyOneChild() {
        return (this.hasLchild() && !this.hasRchild()) || (!this.hasLchild() && this.hasRchild());
    }

    public BinaryTreeNode getFirstChild() {
        if (this.hasLchild())
            return this.getLchild();
        else if (this.hasRchild())
            return this.getRchild();
        else
            return null;
    }

    public void insert(BinaryTreeNode newNode) {
        boolean isLower = newNode.getItem().doubleValue() <= this.getItem().doubleValue();

        if (this.lchild == null && isLower) {
            this.setLchild(newNode);
        } else if (this.rchild == null && !isLower) {
            this.setRchild(newNode);
        } else {
            if (isLower)
                this.getLchild().insert(newNode);
            else
                this.getRchild().insert(newNode);
        }
    }

    public List<BinaryTreeNode> traverseInOrder() {
        List<BinaryTreeNode> nodes = new ArrayList<BinaryTreeNode>();

        if (this.lchild != null) {
            nodes.addAll(this.getLchild().traverseInOrder());
        }

        nodes.add(this);

        if (this.rchild != null) {
            nodes.addAll(this.getRchild().traverseInOrder());
        }

        return nodes;
    }

    public void substituteBySuccessor() {
        if (!this.hasRchild())
            return;

        BinaryTreeNode successorNode = null;
        BinaryTreeNode successorNodeParent = this;
        BinaryTreeNode currNode = this.getRchild();

        while (successorNode == null) {
            if (!currNode.hasLchild()) {
                successorNode = currNode;
            } else {
                successorNodeParent = currNode;
                currNode = currNode.getLchild();
            }
        }

        if (successorNode.hasChildren()) {
            BinaryTreeNode successorNodeChild = successorNode.getRchild();
            if (successorNodeParent.isLchild(successorNode))
                successorNodeParent.setLchild(successorNodeChild);
            else
                successorNodeParent.setRchild(successorNodeChild);
        } else {
            if (successorNodeParent.isLchild(successorNode))
                successorNodeParent.setLchild(null);
            else
                successorNodeParent.setRchild(null);
        }

        this.setItem(successorNode.getItem());
    }
}
