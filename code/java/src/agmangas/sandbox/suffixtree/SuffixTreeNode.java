package agmangas.sandbox.suffixtree;

import java.util.ArrayList;
import java.util.List;

public class SuffixTreeNode {
    private SuffixTree tree;
    private Integer origBeginIdx;
    private Integer beginIdx;
    private Integer endIdx;
    private Integer wordIdx;
    private String debugNodeWord;
    private List<SuffixTreeNode> children;

    public SuffixTreeNode(SuffixTree tree, Integer wordIdx, Integer origBeginIdx,
                          Integer beginIdx, Integer endIdx) {
        this.tree = tree;
        this.origBeginIdx = origBeginIdx;
        this.beginIdx = beginIdx;
        this.endIdx = endIdx;
        this.wordIdx = wordIdx;
        this.children = new ArrayList<SuffixTreeNode>();
        this.updateNodeWord();
    }

    private void updateNodeWord() {
        this.debugNodeWord = this.getNodeWord();
    }

    private static Integer findIntersectionIdx(String word1, String word2) throws NoIntersectionException {
        Integer intersectIdx = 0;

        for (int i = 0; i < word1.length(); i++) {
            if (i < word1.length() && i < word2.length()) {
                if (word1.charAt(0) == word2.charAt(0)) {
                    intersectIdx++;
                } else {
                    break;
                }
            }
        }

        if (intersectIdx == 0)
            throw new NoIntersectionException();
        else
            return intersectIdx;
    }

    public Integer getOrigBeginIdx() {
        return origBeginIdx;
    }

    public void setOrigBeginIdx(Integer origBeginIdx) {
        this.origBeginIdx = origBeginIdx;
    }

    public SuffixTree getTree() {
        return this.tree;
    }

    public List<SuffixTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<SuffixTreeNode> children) {
        this.children = children;
    }

    public Integer getBeginIdx() {
        return beginIdx;
    }

    public Integer getEndIdx() {
        return endIdx;
    }

    public void setEndIdx(Integer endIdx) {
        this.endIdx = endIdx;
        this.updateNodeWord();
    }

    public Integer getWordIdx() {
        return wordIdx;
    }

    public boolean isLeaf() {
        return this.children.size() == 0;
    }

    public String getNodeWord() {
        return this.getTree().getWord(this.getWordIdx(), this.getBeginIdx(), this.getEndIdx());
    }

    private NodeDivision getNodeDivision(Integer newWordIdx, Integer origBeginIdx, Integer newBeginIdx,
                                         Integer newEndIdx) throws NoIntersectionException {
        String nodeWord = this.getNodeWord();
        String newWord = this.getTree().getWord(newWordIdx, newBeginIdx, newEndIdx);
        Integer intersectIdx = SuffixTreeNode.findIntersectionIdx(nodeWord, newWord);

        Integer inheritorChildBeginIdx;
        if (nodeWord.length() > newWord.length())
            inheritorChildBeginIdx = this.getEndIdx() - (nodeWord.length() - newWord.length());
        else
            inheritorChildBeginIdx = this.getEndIdx();

        Integer newChildBeginIdx = newBeginIdx + intersectIdx;

        SuffixTreeNode childInheritor = new SuffixTreeNode(
                this.getTree(), this.getWordIdx(), origBeginIdx, inheritorChildBeginIdx, this.getEndIdx());
        SuffixTreeNode childNew = new SuffixTreeNode(
                this.getTree(), newWordIdx, origBeginIdx, newChildBeginIdx, newEndIdx);

        childInheritor.setOrigBeginIdx(this.getOrigBeginIdx());
        childNew.setOrigBeginIdx(origBeginIdx);

        return new NodeDivision(childInheritor, childNew);
    }

    private void splitDown(Integer newWordIdx, Integer origBeginIdx, Integer newBeginIdx,
                           Integer newEndIdx) throws NoIntersectionException {
        NodeDivision nodeDivision = this.getNodeDivision(newWordIdx, origBeginIdx, newBeginIdx, newEndIdx);
        this.getChildren().add(nodeDivision.getChildInheritor());
        this.getChildren().add(nodeDivision.getChildNew());
        this.setEndIdx(nodeDivision.getChildInheritor().getBeginIdx());
    }

    private void splitUp(Integer newWordIdx, Integer origBeginIdx, Integer newBeginIdx,
                         Integer newEndIdx) throws NoIntersectionException {
        NodeDivision nodeDivision = this.getNodeDivision(newWordIdx, origBeginIdx, newBeginIdx, newEndIdx);
        List<SuffixTreeNode> currChildren = new ArrayList<SuffixTreeNode>(this.getChildren());
        nodeDivision.getChildInheritor().setChildren(currChildren);
        this.getChildren().clear();
        this.getChildren().add(nodeDivision.getChildInheritor());
        this.getChildren().add(nodeDivision.getChildNew());
        this.setEndIdx(nodeDivision.getChildInheritor().getBeginIdx());
    }

    private void addEmptyNode(Integer newWordIdx, Integer origBeginIdx) {
        Integer wordLength = this.getTree().getWord(newWordIdx).length();
        SuffixTreeNode newEmptyNode = new SuffixTreeNode(
                this.getTree(), newWordIdx, origBeginIdx, wordLength, wordLength);
        this.getChildren().add(newEmptyNode);
    }

    public void insert(Integer newWordIdx, Integer origBeginIdx, Integer newBeginIdx,
                       Integer newEndIdx) throws NoIntersectionException {
        if (this.isLeaf()) {
            this.splitDown(newWordIdx, origBeginIdx, newBeginIdx, newEndIdx);
        } else {
            String nodeWord = this.getNodeWord();
            String newWord = this.getTree().getWord(newWordIdx, newBeginIdx, newEndIdx);
            Integer intersectIdx = SuffixTreeNode.findIntersectionIdx(nodeWord, newWord);

            if (nodeWord.length() == intersectIdx && newWord.length() > nodeWord.length()) {
                String subWord = newWord.substring(intersectIdx);
                try {
                    SuffixTreeNode childNode = SuffixTree.findChildMatch(this.getChildren(), subWord);
                    childNode.insert(newWordIdx, origBeginIdx, newBeginIdx + intersectIdx, newEndIdx);
                } catch (NoMatchException e) {
                    SuffixTreeNode newChildNode = new SuffixTreeNode(
                            this.getTree(), newWordIdx, origBeginIdx, newBeginIdx + intersectIdx, newEndIdx);
                    this.getChildren().add(newChildNode);
                }
            } else if (nodeWord.length() == intersectIdx && newWord.length() == nodeWord.length()) {
                this.addEmptyNode(newWordIdx, origBeginIdx);
            } else {
                this.splitUp(newWordIdx, origBeginIdx, newBeginIdx, newEndIdx);
            }
        }
    }

    public SuffixTreeNode searchSubstring(String query, String previous)
            throws NoMatchException {
        String nodeWord = this.getNodeWord();
        String current = previous;

        for (int i = 0; i < nodeWord.length(); i++) {
            current += nodeWord.charAt(i);
            if (query.equals(current)) {
                return this;
            }
        }

        String nextQuery = query.substring(previous.length());
        SuffixTreeNode childNode = SuffixTree.findChildMatch(this.getChildren(), nextQuery);
        return childNode.searchSubstring(query, current);
    }
}
