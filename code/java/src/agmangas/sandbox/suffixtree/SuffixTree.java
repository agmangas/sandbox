package agmangas.sandbox.suffixtree;

import java.util.ArrayList;
import java.util.List;

public class SuffixTree {
    private List<SuffixTreeNode> rootChildren;
    private List<String> words;

    public SuffixTree(List<String> words) {
        this.rootChildren = new ArrayList<SuffixTreeNode>();
        this.words = words;
    }

    public static SuffixTreeNode findChildMatch(List<SuffixTreeNode> children, String newWord)
            throws NoMatchException {
        for (SuffixTreeNode child : children) {
            if (child.getNodeWord().length() > 0 && newWord.length() > 0) {
                if (child.getNodeWord().charAt(0) == newWord.charAt(0))
                    return child;
            }
        }

        throw new NoMatchException();
    }

    public String getWord(Integer wordIdx) {
        return this.words.get(wordIdx);
    }

    public String getWord(Integer wordIdx, Integer beginIdx, Integer endIdx) {
        String treeWord = this.getWord(wordIdx);
        if (beginIdx < treeWord.length())
            return treeWord.substring(beginIdx, endIdx);
        else
            return "";
    }

    private void newRootChild(Integer wordIdx, Integer beginIdx, Integer endIdx) {
        SuffixTreeNode rootChild = new SuffixTreeNode(this, wordIdx, beginIdx, beginIdx, endIdx);
        this.rootChildren.add(rootChild);
    }

    public void build() throws NoIntersectionException {
        String currSuffix;
        String currWord;
        for (int wordIdx = 0; wordIdx < this.words.size(); wordIdx++) {
            currWord = this.words.get(wordIdx);
            for (int beginIdx = 0; beginIdx < currWord.length(); beginIdx++) {
                currSuffix = currWord.substring(beginIdx);
                try {
                    SuffixTreeNode rootChild = SuffixTree.findChildMatch(this.rootChildren, currSuffix);
                    rootChild.insert(wordIdx, beginIdx, beginIdx, currWord.length());
                } catch (NoMatchException e) {
                    this.newRootChild(wordIdx, beginIdx, currWord.length());
                }
            }
        }
    }

    public SuffixTreeNode searchSubstring(String query) throws NoMatchException {
        SuffixTreeNode rootChild = SuffixTree.findChildMatch(this.rootChildren, query);
        return rootChild.searchSubstring(query, "");
    }
}
