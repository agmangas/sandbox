package agmangas.sandbox.suffixtree;

public class NodeDivision {
    private SuffixTreeNode childInheritor;
    private SuffixTreeNode childNew;

    public NodeDivision(SuffixTreeNode childInheritor, SuffixTreeNode childNew) {
        this.childInheritor = childInheritor;
        this.childNew = childNew;
    }

    public SuffixTreeNode getChildInheritor() {
        return childInheritor;
    }

    public SuffixTreeNode getChildNew() {
        return childNew;
    }
}
