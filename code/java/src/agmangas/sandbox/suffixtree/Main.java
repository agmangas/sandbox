package agmangas.sandbox.suffixtree;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String args[]) {
        List<String> words = new ArrayList<String>();
        words.add("ricardio");
        words.add("carmen");
        SuffixTree suffixTree = new SuffixTree(words);
        try {
            suffixTree.build();
        } catch (NoIntersectionException e) {
            e.printStackTrace();
        }

        try {
            suffixTree.searchSubstring("car");
        } catch (NoMatchException e) {
            e.printStackTrace();
        }
    }
}
