<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/24/15
 * Time: 2:05 PM
 */

namespace Agmangas\Sandbox\Patterns\Template;


class IceCreamCoffeeMaker extends CoffeeMakerTemplate
{

    protected function brewCoffee()
    {
        return "Brew a light and fruity coffee";
    }

    protected function mixSugar()
    {
        return "Add two teaspoons of sugar";
    }

    protected function addTopping()
    {
        return "Add a scoop of vanilla ice cream";
    }
}