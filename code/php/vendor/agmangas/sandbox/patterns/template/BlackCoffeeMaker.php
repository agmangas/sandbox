<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/24/15
 * Time: 2:03 PM
 */

namespace Agmangas\Sandbox\Patterns\Template;


class BlackCoffeeMaker extends CoffeeMakerTemplate
{

    protected function brewCoffee()
    {
        return "Brew a strong black colombian coffee";
    }

    protected function mixSugar()
    {
        return "Add one teaspoon of sugar";
    }

    protected function addTopping()
    {
        return "No toppings required";
    }
}