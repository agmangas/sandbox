<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/24/15
 * Time: 1:58 PM
 */

namespace Agmangas\Sandbox\Patterns\Template;


abstract class CoffeeMakerTemplate
{
    public function makeCoffee()
    {
        $coffee = "Let's make a coffee!\n";
        $coffee .= $this->brewCoffee() . "\n";
        $coffee .= $this->mixSugar() . "\n";
        $coffee .= $this->addTopping() . "\n";

        return $coffee;
    }

    protected abstract function brewCoffee();

    protected abstract function mixSugar();

    protected abstract function addTopping();
} 