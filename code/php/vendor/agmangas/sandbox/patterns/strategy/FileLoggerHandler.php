<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:19 PM
 */

namespace Agmangas\Sandbox\Patterns\Strategy;


class FileLoggerHandler implements ILoggerHandler
{
    public function message($msg)
    {
        echo(sprintf("[File]: %s\n", $msg));
    }
} 