<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:18 PM
 */

namespace Agmangas\Sandbox\Patterns\Strategy;


class Logger
{
    private $handlerStrategy;

    public function __construct(ILoggerHandler $handlerStrategy)
    {
        $this->handlerStrategy = $handlerStrategy;
    }

    public function setHandlerStrategy(ILoggerHandler $handlerStrategy)
    {
        $this->handlerStrategy = $handlerStrategy;
    }

    public function message($msg)
    {
        $this->handlerStrategy->message($msg);
    }
} 