<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:18 PM
 */

namespace Agmangas\Sandbox\Patterns\Strategy;


interface ILoggerHandler
{
    public function message($msg);
} 