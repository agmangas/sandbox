<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:19 PM
 */

namespace Agmangas\Sandbox\Patterns\Strategy;


class StdoutLoggerHandler implements ILoggerHandler
{
    public function message($msg)
    {
        echo(sprintf("[Stdout]: %s\n", $msg));
    }
}