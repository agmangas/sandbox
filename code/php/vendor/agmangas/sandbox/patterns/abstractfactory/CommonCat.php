<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:10 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


class CommonCat extends AbstractCat
{
    public function meow()
    {
        return sprintf("Miu!");
    }

    public function play()
    {
        return sprintf("Run and jump and bite and scratch!");
    }
}