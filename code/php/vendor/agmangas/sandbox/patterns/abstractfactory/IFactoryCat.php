<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:09 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


interface IFactoryCat
{
    public function createCat($name);
} 