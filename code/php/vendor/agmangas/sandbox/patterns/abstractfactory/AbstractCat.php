<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:09 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


abstract class AbstractCat
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public abstract function meow();

    public abstract function play();
} 