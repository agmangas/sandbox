<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:12 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


class FactoryCommonCat implements IFactoryCat
{
    public function createCat($name)
    {
        return new CommonCat($name);
    }
}