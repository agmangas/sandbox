<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:09 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


class SnowshoeCat extends AbstractCat
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->name = sprintf("Lady %s", $this->name);
    }

    public function meow()
    {
        return sprintf("Meaow...");
    }

    public function play()
    {
        return sprintf("A true snowshoe doesn't play!");
    }
}