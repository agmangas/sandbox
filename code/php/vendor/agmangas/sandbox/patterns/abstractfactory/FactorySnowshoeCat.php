<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:11 PM
 */

namespace Agmangas\Sandbox\Patterns\Abstractfactory;


class FactorySnowshoeCat implements IFactoryCat
{
    public function createCat($name)
    {
        return new SnowshoeCat($name);
    }
}