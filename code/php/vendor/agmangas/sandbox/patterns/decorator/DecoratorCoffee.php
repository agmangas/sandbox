<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:33 PM
 */

namespace Agmangas\Sandbox\Patterns\Decorator;


abstract class DecoratorCoffee implements ICoffee
{
    protected $decorated;

    public function __construct($decorated)
    {
        $this->decorated = $decorated;
    }

    public function __call($name, $arguments)
    {
        if (!in_array($name, get_class_methods($this))) {
            call_user_func_array(array($this->decorated, $name), $arguments);
        }
    }

    abstract public function drink();

    abstract public function cost();
} 