<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:41 PM
 */

namespace Agmangas\Sandbox\Patterns\Decorator;


class CreamDecoratorCoffee extends DecoratorCoffee
{
    const COST = 2;

    public function drink()
    {
        $baseDrink = $this->decorated->drink();
        return sprintf("%s (and cream)", $baseDrink);
    }

    public function cost()
    {
        $baseCost = $this->decorated->cost();
        return $baseCost + CreamDecoratorCoffee::COST;
    }
}