<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:23 PM
 */

namespace Agmangas\Sandbox\Patterns\Decorator;


class SimpleCoffee implements ICoffee
{
    const COST = 4;

    private $ingredients;

    public function __construct()
    {
        $this->ingredients = array("coffee");
    }

    public function drink()
    {
        return sprintf("I'm drinking a coffee with: %s", implode(", ", $this->ingredients));
    }

    public function cost()
    {
        return SimpleCoffee::COST;
    }

    public function addIngredient($newIngredient)
    {
        $this->ingredients[] = $newIngredient;
    }
}