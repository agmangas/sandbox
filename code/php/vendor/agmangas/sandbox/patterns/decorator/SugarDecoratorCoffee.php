<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:46 PM
 */

namespace Agmangas\Sandbox\Patterns\Decorator;


class SugarDecoratorCoffee extends DecoratorCoffee
{
    const COST = 1;

    public function drink()
    {
        $baseDrink = $this->decorated->drink();
        return sprintf("%s (and sugar)", $baseDrink);
    }

    public function cost()
    {
        $baseCost = $this->decorated->cost();
        return $baseCost + SugarDecoratorCoffee::COST;
    }

} 