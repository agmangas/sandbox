<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:22 PM
 */

namespace Agmangas\Sandbox\Patterns\Decorator;


interface ICoffee
{
    public function drink();

    public function cost();
} 