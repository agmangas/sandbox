<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:48 PM
 */

namespace Agmangas\Sandbox\Patterns\Adapter;


class AdaptorCalculator
{
    private $adaptee;

    public function __construct()
    {
        $this->adaptee = new AdapteeCalculator(null, null);
    }

    public function sum($x, $y)
    {
        $this->adaptee->setX($x);
        $this->adaptee->setY($y);

        return $this->adaptee->sum();
    }
} 