<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:57 PM
 */

namespace Agmangas\Sandbox\Patterns\Adapter;


class CalculatorClient
{
    private $calculator;

    public function __construct(AdaptorCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function groupSum($numArr)
    {
        $ret = 0;

        foreach ($numArr as $num) {
            $ret = $this->calculator->sum($ret, $num);
        }

        return $ret;
    }
} 