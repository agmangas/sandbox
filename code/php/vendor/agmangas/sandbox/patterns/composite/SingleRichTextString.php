<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 7:29 PM
 */

namespace Agmangas\Sandbox\Patterns\Composite;


class SingleRichTextString implements IRichTextString
{
    private $char;

    public function __construct($char)
    {
        $this->char = $char;
    }

    public function uppercase()
    {
        $this->char = strtoupper($this->char);
    }

    public function __toString()
    {
        return sprintf("%s\n", $this->char);
    }
}