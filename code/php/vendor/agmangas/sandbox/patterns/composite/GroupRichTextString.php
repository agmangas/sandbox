<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 7:35 PM
 */

namespace Agmangas\Sandbox\Patterns\Composite;


class GroupRichTextString implements IRichTextString
{
    private $strings;

    public function __construct()
    {
        $this->strings = array();
    }

    public function uppercase()
    {
        foreach ($this->strings as $string) {
            $string->uppercase();
        }
    }

    public function addString(IRichTextString $string)
    {
        $this->strings[] = $string;
    }

    public function __toString()
    {
        $ret = "";

        foreach ($this->strings as $string) {
            $ret .= sprintf("%s", $string);
        }

        return $ret;
    }
}