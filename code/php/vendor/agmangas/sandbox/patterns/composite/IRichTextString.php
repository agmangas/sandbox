<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 7:27 PM
 */

namespace Agmangas\Sandbox\Patterns\Composite;


interface IRichTextString
{
    public function uppercase();
} 