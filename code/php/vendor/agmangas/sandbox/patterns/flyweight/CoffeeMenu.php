<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 3:32 PM
 */

namespace Agmangas\Sandbox\Patterns\Flyweight;


class CoffeeMenu
{
    private $flavours;

    public function __construct()
    {
        $this->flavours = array();
    }

    public function getFlavour($flavour)
    {
        if (!in_array($flavour, $this->flavours)) {
            $this->flavours[$flavour] = new CoffeeFlavour($flavour);
        }

        return $this->flavours[$flavour];
    }
} 