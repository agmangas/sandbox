<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 3:16 PM
 */

namespace Agmangas\Sandbox\Patterns\Flyweight;

class CoffeeFlavoursEnum
{
    const LATTE = "latte";
    const BLACK = "black";
    const CAPUCCINO = "capuccino";

    public static function costs()
    {
        return array(
            CoffeeFlavoursEnum::LATTE => 3,
            CoffeeFlavoursEnum::BLACK => 2,
            CoffeeFlavoursEnum::CAPUCCINO => 6
        );
    }

    public static function sizes()
    {
        return array(
            CoffeeFlavoursEnum::LATTE => "L",
            CoffeeFlavoursEnum::BLACK => "M",
            CoffeeFlavoursEnum::CAPUCCINO => "XL"
        );
    }
}

class CoffeeFlavour
{
    private $name;
    private $cost;
    private $size;

    public function __construct($name)
    {
        $this->name = $name;
        $this->cost = CoffeeFlavoursEnum::costs()[$name];
        $this->size = CoffeeFlavoursEnum::sizes()[$name];
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }
} 