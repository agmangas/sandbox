<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:02 PM
 */

namespace Agmangas\Sandbox\Patterns\Chainresp;


abstract class Logger
{
    private $minLevel;

    public function __construct($minLevel)
    {
        $this->minLevel = $minLevel;
    }

    public function getMinLevel()
    {
        return $this->minLevel;
    }

    public abstract function message($msg, $level);
} 