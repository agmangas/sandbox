<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:04 PM
 */

namespace Agmangas\Sandbox\Patterns\Chainresp;


class FileLogger extends Logger
{
    public function message($msg, $level)
    {
        if ($level >= $this->getMinLevel()) {
            echo(sprintf("[File]: %s\n", $msg));
        }
    }
} 