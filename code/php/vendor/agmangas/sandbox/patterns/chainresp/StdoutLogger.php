<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:02 PM
 */

namespace Agmangas\Sandbox\Patterns\Chainresp;


class StdoutLogger extends Logger
{
    public function message($msg, $level)
    {
        if ($level >= $this->getMinLevel()) {
            echo(sprintf("[Stdout]: %s\n", $msg));
        }
    }
}