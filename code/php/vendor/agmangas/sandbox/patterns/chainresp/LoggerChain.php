<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:00 PM
 */

namespace Agmangas\Sandbox\Patterns\Chainresp;


class LoggerChain
{
    private $handlers;

    public function __construct()
    {
        $this->handlers = array();
    }

    public function addHandler(Logger $handler)
    {
        $this->handlers[] = $handler;
    }

    public function message($msg, $level)
    {
        foreach ($this->handlers as $handler) {
            $handler->message($msg, $level);
        }
    }
} 