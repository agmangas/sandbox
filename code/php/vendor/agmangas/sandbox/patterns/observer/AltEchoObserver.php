<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 21/05/15
 * Time: 23:45
 */

namespace Agmangas\Sandbox\Patterns\Observer;


class AltEchoObserver implements IObserver
{
    public function notify()
    {
        $line = func_get_arg(0);
        echo(sprintf("[ALT] Observed: %s\n", $line));
    }

} 