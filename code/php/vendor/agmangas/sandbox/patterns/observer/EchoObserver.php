<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 21/05/15
 * Time: 23:40
 */

namespace Agmangas\Sandbox\Patterns\Observer;


class EchoObserver implements IObserver
{
    public function notify()
    {
        $line = func_get_arg(0);
        echo(sprintf("Observed: %s\n", $line));
    }
}