<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 21/05/15
 * Time: 23:58
 */

namespace Agmangas\Sandbox\Patterns\Observer;


class ReadlineObservable extends Observable
{
    public function read()
    {
        $line = readline("To be observed>> ");
        $this->notifyObservers($line);
    }
} 