<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 21/05/15
 * Time: 23:48
 */

namespace Agmangas\Sandbox\Patterns\Observer;


class Observable
{
    private $observers;

    public function __construct()
    {
        $this->observers = array();
    }

    public function addObserver($observer)
    {
        $this->observers[] = $observer;
    }

    public function removeObserver($observer)
    {
        $found = array_search($observer, $this->observers);

        if ($found !== false) {
            unset($this->observers[$found]);
        }
    }

    protected function getObservers()
    {
        return $this->observers;
    }

    public function notifyObservers($notifiedValue)
    {
        foreach ($this->observers as $observer) {
            $observer->notify($notifiedValue);
        }
    }
}