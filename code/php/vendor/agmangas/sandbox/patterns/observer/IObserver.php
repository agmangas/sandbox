<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 21/05/15
 * Time: 23:31
 */

namespace Agmangas\Sandbox\Patterns\Observer;


interface IObserver
{
    public function notify();
} 