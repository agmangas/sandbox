<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 7:39 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Composite as Composite;

$strings = array(
    new Composite\SingleRichTextString("hello world"),
    new Composite\SingleRichTextString("my string"),
    new Composite\SingleRichTextString("curry rice"),
    new Composite\SingleRichTextString("my little cat")
);

$stringGroup = new Composite\GroupRichTextString();

foreach ($strings as $string) {
    $stringGroup->addString($string);
}

echo(sprintf("** Original\n"));

foreach ($strings as $string) {
    echo $string;
}

echo(sprintf("** First capitalized\n"));

$strings[0]->uppercase();

foreach ($strings as $string) {
    echo $string;
}

echo(sprintf("** Group capitalized\n"));

$stringGroup->uppercase();

foreach ($strings as $string) {
    echo $string;
}
