<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 9:02 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Adapter as Adapter;

$adaptorCalculator = new Adapter\AdaptorCalculator();
$calculatorClient = new Adapter\CalculatorClient($adaptorCalculator);

echo(sprintf("** Result sum: %s\n", $calculatorClient->groupSum(array(2, 2, 4))));