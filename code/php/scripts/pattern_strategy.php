<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:24 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Strategy as Strategy;

$handlerFile = new Strategy\FileLoggerHandler();
$handlerStdout = new Strategy\StdoutLoggerHandler();

$logger = new Strategy\Logger($handlerFile);
$logger->message("First message (file handler)");
$logger->setHandlerStrategy($handlerStdout);
$logger->message("Second message (stdout handler)");
$logger->setHandlerStrategy($handlerFile);
$logger->message("Third message (file handler)");