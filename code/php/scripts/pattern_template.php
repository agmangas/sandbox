<?php

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Template as Template;

$blackCoffeeMaker = new Template\BlackCoffeeMaker();
$iceCreamCoffeeMaker = new Template\IceCreamCoffeeMaker();

echo("*** Black coffee\n");
echo($blackCoffeeMaker->makeCoffee());
echo("*** Ice cream coffee\n");
echo($iceCreamCoffeeMaker->makeCoffee());