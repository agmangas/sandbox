<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/23/15
 * Time: 1:47 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Decorator as Decorator;

$myCoffee = new Decorator\SimpleCoffee();
$myCoffeeSugar = new Decorator\SugarDecoratorCoffee($myCoffee);
$myCoffeeSugarCream = new Decorator\CreamDecoratorCoffee($myCoffeeSugar);
$myCoffeeCream = new Decorator\CreamDecoratorCoffee($myCoffee);

echo(sprintf(">> Base coffee cost: %s\n", $myCoffee->cost()));
echo(sprintf(">> Coffee + Sugar cost: %s\n", $myCoffeeSugar->cost()));
echo(sprintf(">> Coffee + Sugar + Cream cost: %s\n", $myCoffeeSugarCream->cost()));
echo(sprintf(">> Coffee + Cream cost: %s\n", $myCoffeeCream->cost()));
echo(sprintf(">> Drinking complete coffee: %s\n", $myCoffeeSugarCream->drink()));
echo(sprintf(">> Adding a new coffee ingredient\n"));

$myCoffeeCream->addIngredient("cinnammon");

echo(sprintf(">> Drinking sugar coffee: %s\n", $myCoffeeSugar->drink()));

