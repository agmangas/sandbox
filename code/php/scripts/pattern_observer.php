<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 22/05/15
 * Time: 00:09
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Observer as Observer;

$observerOne = new Observer\AltEchoObserver();
$observerTwo = new Observer\EchoObserver();
$readlineObservable = new Observer\ReadlineObservable();
$readlineObservable->addObserver($observerOne);
$readlineObservable->addObserver($observerTwo);

foreach (range(0, 2) as $step) {
    $readlineObservable->read();
}