<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/26/15
 * Time: 4:51 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Abstractfactory as Abstractfactory;

$factorySnowshoe = new Abstractfactory\FactorySnowshoeCat();
$factoryCommon = new Abstractfactory\FactoryCommonCat();

$sobi = $factorySnowshoe->createCat("Sobi");
$leela = $factoryCommon->createCat("Leela");

echo(sprintf(">> %s\n", $sobi->meow()));
echo(sprintf(">> %s\n", $sobi->play()));
echo(sprintf(">> %s\n", $leela->meow()));
echo(sprintf(">> %s\n", $leela->play()));