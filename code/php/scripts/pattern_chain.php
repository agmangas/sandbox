<?php
/**
 * Created by PhpStorm.
 * User: agmangas
 * Date: 5/25/15
 * Time: 8:11 PM
 */

require_once("autoload.php");

use \Agmangas\Sandbox\Patterns\Chainresp as Chainresp;

$handlerStdout = new Chainresp\StdoutLogger(1);
$handlerFile = new Chainresp\FileLogger(2);

$loggerChain = new Chainresp\LoggerChain();
$loggerChain->addHandler($handlerStdout);
$loggerChain->addHandler($handlerFile);

$loggerChain->message("First message", 0);
$loggerChain->message("Second message", 2);
$loggerChain->message("Third message", 1);
$loggerChain->message("Fourth message", 0);