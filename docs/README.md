# Data Structures

## Arrays vs Linked Lists

Operation | Linked List | Array | Dynamic Array
--- | --- | --- | ---
Indexing | O(n) | O(1) | O(1)
Insert (beginning) | O(1) | - | O(n)
Insert (middle) | Search + O(1) | - | O(n)
Insert (end) | O(1) *if known* | - | O(1) *amortized*

### Advantages of Linked Lists

* Overflow on linked structures can never occur unless the memory is actually full.
* Insertions and deletions are simpler than for contiguous (array) lists.
* With large records, moving pointers is easier and faster than moving the items themselves.

### Advantages of Arrays

* Linked structures require extra space for storing pointer fields.
* Linked lists do not allow efficient random access to items.
* Arrays allow better memory locality and cache performance than random pointer jumping.