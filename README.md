# Andres Mangas' Sandbox

## Introduction

This repository contains: 

* Miscellaneous docs.
* Data structures & algorithms implementations.
* Experiments.

Which is basically every piece of code or documentation that I may find interesting and worthy of versioning.